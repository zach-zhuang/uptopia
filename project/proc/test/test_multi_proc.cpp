//
// Created by zoson on 18-4-7.
//

#include <iostream>
#include "proc/multi_proc.hpp"
#include <zconf.h>

using namespace std;
using namespace utopia;

class Process1:public Process
{
public:
    Process1(int argc,char** argv):Process(argc,argv) {}
    virtual int main(const std::vector<std::string>& args)
    {
        cout<<getName()<< " Pid "<<getPid()<<" getpid "<<getpid()<<" ";
        for(int i=0;i<args.size();++i)
        {
            cout<<"arg["<<i<<"]="<<args[i]<<" ";
        }
        cout<<endl;
        return 0;
    }
};

class Task1:public Task
{
public:
    virtual int main(const std::vector<std::string>& args)
    {
        cout<<"Task ";
        for(int i=0;i<args.size();++i)
        {
            cout<<"arg["<<i<<"]="<<args[i]<<" ";
        }
        cout<<endl;
        return 0;
    }
};

int main()
{
    MultiProc proc;
    char* (argv[]) = {"P1","hello","world"};
    std::shared_ptr<Process> p1(new Process1(3,argv));
    char* (argv2[]) = {"P2","hello","world"};
    std::shared_ptr<Process> p2(new Process1(3,argv2));
    char* (argv3[]) = {"P3","hello","world"};
    std::shared_ptr<Process> p3(new Process(3,argv3,std::shared_ptr<Task>(new Task1)));
    proc.create(p1);
    proc.create(p2);
    proc.create(p3);
    proc.waitAll();
    cout<<"Pid "<<getpid()<<endl;
    return 0;
}