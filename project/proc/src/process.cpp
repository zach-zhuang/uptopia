//
// Created by zoson on 18-4-7.
//

#include <proc/process.hpp>

namespace utopia
{

Process::Process(int argc, char **argv, std::string name,std::shared_ptr<Task> task ):
        name_(name),task_(task)
{
    for(int i=0;i<argc;++i)
    {
        std::string str(argv[i]);
        args_.push_back(str);
    }
}

Process::Process(int argc,char** argv,std::shared_ptr<Task> task ):
        Process(argc,argv,"main",task){}


Process::Process(std::string name,std::shared_ptr<Task> task ):
        Process(0,NULL,name,task){}

Process::Process(std::shared_ptr<Task> task ):
        Process(0,NULL,"main",task){}

}