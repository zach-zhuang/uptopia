//
// Created by zoson on 18-4-6.
//

#include <proc/multi_proc.hpp>
#include <base/def.hpp>
#include<sys/wait.h>
namespace utopia
{

MultiProc::MultiProc() {}

MultiProc::~MultiProc()
{
    for(ProcessMapIterator it = procs_.begin();it != procs_.end();++it)
    {
        kill(it->second->pid_,SIGKILL);
        //waitpid(it->second->pid_,NULL,WNOHANG);
        procs_.erase(it);
    }
}

void MultiProc::create(std::shared_ptr<Process> t)
{
    CHECK(t.get()!=NULL);
    if(t.get()==NULL)
    {
        LOG(ERROR)<<"Process is invalid to be created";
        return;
    }
    pid_t pid = fork();
    if(pid==0)
    {
        t->pid_ = getpid();
        int ret = -1;
        if(t->task_!= nullptr)
        {
            ret = t->task_->main(t->args_);
        }else{
            ret = t->main(t->args_);
        }
        exit(ret);
    }else if(pid != -1){
        t->pid_ = pid;
        procs_.insert(ProcessPair(t.get(),t));
    }
}

void MultiProc::waitAll()
{
    for(ProcessMapIterator it = procs_.begin();it != procs_.end();++it)
    {
        waitpid(it->second->pid_,NULL,0);
        procs_.erase(it);
    }
}

void MultiProc::wait(std::shared_ptr<Process> t)
{
    ProcessMapIterator it = procs_.find(t.get());
    if(it == procs_.end())
    {
        LOG(ERROR)<<"Process doesn't exist";
        return;
    }
    procs_.erase(it);
    waitpid(t->pid_,NULL,0);
}

}
