//
// Created by zoson on 18-4-7.
//

#ifndef UTOPIA_PROCESS_HPP
#define UTOPIA_PROCESS_HPP

#include <string>
#include <vector>
#include <memory>

namespace utopia
{

class MultiProc;

class Task
{
public:
    virtual int main(const std::vector<std::string>& args)=0;
};

class Process
{
    friend MultiProc;
public:
    Process():task_(nullptr),name_("main"){};
    Process(int argc,char** argv,std::string name,std::shared_ptr<Task> task = nullptr);
    Process(int argc,char** argv,std::shared_ptr<Task> task = nullptr);
    Process(std::string name,std::shared_ptr<Task> task = nullptr);
    Process(std::shared_ptr<Task> task);
    virtual int main(const std::vector<std::string>& args){return 0;};
    inline pid_t getPid(){return pid_;}
    inline std::string getName(){return name_;}
private:
    pid_t pid_;
    std::string name_;
    std::vector<std::string> args_;
    std::shared_ptr<Task> task_;
};

}

#endif //UTOPIA_PROCESS_HPP
