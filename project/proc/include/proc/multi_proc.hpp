//
// Created by zoson on 18-4-6.
//

#ifndef UTOPIA_MULTI_PROC_HPP
#define UTOPIA_MULTI_PROC_HPP

#include <memory>
#include <map>
#include "process.hpp"
namespace utopia
{

typedef std::map<Process*,std::shared_ptr<Process> > ProcessMap;
typedef std::map<Process*,std::shared_ptr<Process> >::iterator ProcessMapIterator;
typedef std::pair<Process*,std::shared_ptr<Process> > ProcessPair;

class MultiProc
{
public:
    MultiProc();
    ~MultiProc();
    void create(std::shared_ptr<Process> t);
    void waitAll();
    void wait(std::shared_ptr<Process> t);
private:
    ProcessMap procs_;
};

}

#endif //UTOPIA_MULTI_FORK_HPP
