cmake_minimum_required(VERSION 3.9)
project(proc)

set(CMAKE_CXX_STANDARD 11)
set(ROOT ../..)
set(UTOPIA_PROC_LINK_LIBS pthread)

set(UTOPIA_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)
include_directories(${UTOPIA_INCLUDE_DIR})

#GTest
include(${ROOT}/cmake/gtest.cmake)
FIND_GTEST(GTEST_BOTH_LIBRARIES)
list(APPEND UTOPIA_PROC_LINK_LIBS ${GTEST_BOTH_LIBRARIES})

#GLog
include(${ROOT}/cmake/glog.cmake)
FIND_GLOG(GLOG_BOTH_LIBRARIES)
list(APPEND UTOPIA_PROC_LINK_LIBS ${GLOG_BOTH_LIBRARIES})

include(${ROOT}/cmake/boost.cmake)
FIND_BOOST(BOOST_BOTH_LIBRARIES)
list(APPEND UTOPIA_PROC_LINK_LIBS ${BOOST_BOTH_LIBRARIES})

file(GLOB_RECURSE CPP_FILES "${PROJECT_SOURCE_DIR}/src/*.cpp")
file(GLOB_RECURSE HPP_FILES "${PROJECT_SOURCE_DIR}/include/*.hpp")
add_library(utopia_proc SHARED STATIC ${CPP_FILES} ${HPP_FILES})

list(APPEND UTOPIA_PROC_LINK_LIBS utopia_base)
include(${ROOT}/cmake/utils.cmake)
IMPORT(${ROOT}/project/base utopia_base)
list(REVERSE UTOPIA_PROC_LINK_LIBS)

# test
file(GLOB_RECURSE TEST_FILES "${PROJECT_SOURCE_DIR}/test/*.cpp")
foreach(FILE ${TEST_FILES})
    get_filename_component(FIL ${FILE} NAME_WE)
    add_executable(${FIL} ${FILE} utopia_base)
    target_link_libraries(${FIL} utopia_proc ${UTOPIA_PROC_LINK_LIBS})
endforeach()