//
// Created by zoson on 18-3-25.
//

#ifndef UTOPIA_TEST_DEF_HPP
#define UTOPIA_TEST_DEF_HPP

#define test_arr_arr(data1,data2,len) EXPECT_TRUE(data1!=NULL); \
    EXPECT_TRUE(data2!=NULL); \
    EXPECT_GT(len,0); \
    for(size_t i=0;i<len;++i) \
    { \
        EXPECT_TRUE(data1[i]==data2[i]);\
    }

#define test_arr(data,datum,len) EXPECT_TRUE(data!=NULL);\
    EXPECT_GT(len,0);\
    for(size_t i = 0;i<len;++i)\
    {\
        EXPECT_TRUE(data[i]==datum);\
    }

#define set_arr(data,len,datum) EXPECT_TRUE(data!=NULL);\
    EXPECT_GT(len,0);\
    for(size_t i=0;i<len;++i)\
    {\
        data[i] = datum;\
    }

#endif //UTOPIA_TEST_DEF_HPP
