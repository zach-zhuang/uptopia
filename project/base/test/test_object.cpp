//
// Created by zoson on 18-3-30.
//

#include <base/object.hpp>
#include <iostream>
#include <thread>
using namespace std;

utopia::Object ob;

void thread_wait1()
{
    while(true)
    {
        cout<<"thread_wait1"<<endl;
        ob.wait();
    }
}

void thread_wait2()
{
    while(true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        cout<<"thread_wait2"<<endl;
        ob.wait();
    }
}

void thread_wait3()
{
    while(true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(400));
        cout<<"thread_wait3"<<endl;
        ob.wait();
    }
}

void thread_notify()
{
    while(true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        cout<<"thread_notify"<<endl;
        ob.notify();
    }
}

void thread_syn1()
{
    while(true)
    {
        {
            Sychronized(&ob)
            cout<<"thread_syn1"<<endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        //std::this_thread::sleep_for(std::chrono::milliseconds(300));
    }
}

void thread_syn2()
{
    while(true)
    {
        {
            Sychronized(&ob)
            cout<<"thread_syn2"<<endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(300));
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(600));

    }
}

int main()
{
    thread wait1(thread_wait1);
    thread wait2(thread_wait2);
    thread wait3(thread_wait3);
    thread notify(thread_notify);
    thread syn2(thread_syn2);
    thread syn1(thread_syn1);

    wait1.join();
    wait2.join();
    wait3.join();
    notify.join();
    syn1.join();
    syn2.join();
}