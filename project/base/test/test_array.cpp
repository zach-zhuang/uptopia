//
// Created by zoson on 18-3-24.
//

#include <iostream>
#include <base/array.hpp>
#include <gtest/gtest.h>
#include "test_def.hpp"
using namespace utopia;

TEST(TestArrayCase,TestArrayConstrutor)
{
    //test construtor
    Array<float> floats(10);
    EXPECT_EQ(floats.size(),10);
    EXPECT_TRUE(floats.data()!=NULL);

    //test constructor  copy == true
    float* copy_floats = new float[30];
    set_arr(copy_floats,30,1.2f);
    Array<float> floats2(copy_floats,30,true);
    EXPECT_TRUE(floats2.data()!=copy_floats);
    EXPECT_EQ(floats2.size(),30);
    test_arr_arr(floats2.data(),copy_floats,30);

    Array<float> floats3(copy_floats,30);
    EXPECT_TRUE(floats3.data()==copy_floats);
    EXPECT_EQ(floats3.size(),30);
    test_arr_arr(floats3.data(),copy_floats,30);

    //delete[] copy_floats;
}

TEST(TestArrayCase,TestArrayAssign)
{
    Array<float> floats(10);
    float* float_ptr = new float[20];
    floats.assign(float_ptr,20);
    EXPECT_EQ(floats.size(),20);
    EXPECT_TRUE(floats.data()==float_ptr);
    //delete[] float_ptr;
}

TEST(TestArrayCase,TestArraySet)
{
    Array<float> floats(20);
    float* float_ptr = new float[20];
    set_arr(float_ptr,20,1.0f);
    //test set
    floats.set(1.0f);
    test_arr(floats.data(),1.0f,floats.size());
    test_arr_arr(floats.data(),float_ptr,floats.size());
    delete[] float_ptr;
}

TEST(TestArrayCase,TestArrayCopyTo)
{
    //Dtype *data
    Array<float> floats(30);
    float* float_ptr = new float[30];
    //set_arr(float_ptr,30,1.2f);
    floats.set(1.2f);
    floats.copyTo(float_ptr);
    test_arr_arr(floats.data(),float_ptr,30);
    delete[] float_ptr;

    //Dtype **data
    float* float_ptr2 =NULL;
    floats.copyTo(&float_ptr2);
    EXPECT_TRUE(float_ptr2!=NULL);
    test_arr_arr(float_ptr2,floats.data(),30);
    delete[] float_ptr2;
}

TEST(TestArrayCase,TestArrayToPtr)
{
    Array<float> floats(30);
    floats.set(1.33f);
    size_t len =0;
    float* float_ptr = floats.toPtr(len);
    EXPECT_TRUE(floats.data()==NULL);
    EXPECT_EQ(floats.size(),0);
    EXPECT_EQ(len,30);
    test_arr(float_ptr,1.33f,len);
    delete[] float_ptr;
}


TEST(TestArrayCase,TestArrayCopyFrom)
{
    float float_ptr[30];
    set_arr(float_ptr,30,1.33f);
    Array<float> floats(10);
    floats.copyFrom(float_ptr,30);
    EXPECT_EQ(floats.size(),30);
    test_arr(floats.data(),1.33f,floats.size());
}

TEST(TestArrayCase,TestArrayMutable_Data_Size)
{
    Array<float> floats;
    EXPECT_EQ(floats.size(),0);
    EXPECT_TRUE(floats.data()==NULL);
    float**ptr_mutable = &(floats.data_mutable());
    *ptr_mutable = new float[20];
    floats.size_mutable() = 20;
    EXPECT_EQ(floats.size(),20);
    EXPECT_TRUE(floats.data()!=NULL);
}

int main(int argc,char** argv)
{
    ::testing::InitGoogleTest(&argc,argv);
    return RUN_ALL_TESTS();
}