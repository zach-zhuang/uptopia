#include <iostream>
#include <gtest/gtest.h>
#include <base/bundle.hpp>
#include <base/mem_buffer.hpp>
using namespace std;

namespace utopia
{
template<typename T> 
int compareArr(const T* a1,const T* a2,size_t len)
{
	for(int i=0;i<len;++i)
	{
		if(a1[i]!=a2[i])
		{
			return -1;
		}
	}
	return 0;
}

//test buffer
class MemBufferTest:public ::testing::Test
{
protected:
	virtual void setUp(){}
};

TEST_F(MemBufferTest,writeAndRead)
{
	MemBuffer buf_w;
	size_t size = 1024*1024;
	Byte *cont = new Byte[size];
	for(size_t i=0;i<size;++i)
	{
		cont[i] = i%10 + 'a';
	}
	buf_w.write(cont,size);
	EXPECT_EQ(buf_w.getSize(),size);
	EXPECT_EQ(0,compareArr(cont,buf_w.getMutableData(),buf_w.getSize()));

	Byte* cont_r = new Byte[size];
	buf_w.read(cont_r,size);
	EXPECT_EQ(0,compareArr(cont_r,cont,size));

	MemBuffer buf_c;
	buf_c.copy(buf_w);
	EXPECT_EQ(buf_c.getSize(),size);
	EXPECT_EQ(compareArr(buf_c.getConstData(),cont,size),0);

	MemBuffer buf_s;
	buf_s.resize(size);
	EXPECT_EQ(size,buf_s.getSize());
	buf_s.resize(size*2);
	EXPECT_EQ(size*2,buf_s.getSize());
	buf_s.write(cont,size);
	buf_s.write(cont,size);
	EXPECT_EQ(size*2,buf_s.getSize());

	Byte* cont_c = new Byte[size*2];
	buf_s.read(cont_c,size*2);
	EXPECT_EQ(0,compareArr(cont_c,buf_s.getConstData(),size*2));

	MemBuffer buf_memcpy;
	buf_memcpy.resize(size*2);
	memcpy(buf_memcpy.getMutableData(),cont_c,buf_memcpy.getSize());
	EXPECT_EQ(0,compareArr(cont_c,buf_memcpy.getConstData(),size*2));

	MemBuffer buf_seek;
	EXPECT_EQ(0,buf_seek.seekg(10));
	EXPECT_EQ(0,buf_seek.seekp(10));
	EXPECT_EQ(0,buf_seek.tellg());
	EXPECT_EQ(0,buf_seek.tellp());
	buf_seek.write(cont,size);
	EXPECT_EQ(size,buf_seek.tellg());
	EXPECT_EQ(10,buf_seek.seekg(10));
	EXPECT_EQ(size-10,buf_seek.seekg(10,IBuffer::Pos::end));
	EXPECT_EQ(10,buf_seek.seekp(10));
	EXPECT_EQ(size-10,buf_seek.seekp(10,IBuffer::Pos::end));
	EXPECT_EQ(size,buf_seek.seekg(10,IBuffer::Pos::cur));
	EXPECT_EQ(size,buf_seek.seekp(10,IBuffer::Pos::cur));
	EXPECT_EQ(0,buf_seek.seekp(0));
	Byte* cont_read_from = new Byte[size];
	buf_seek.read(cont_read_from,size);
	EXPECT_EQ(0,buf_seek.tellp());
	EXPECT_EQ(0,compareArr(cont_read_from,cont,size));

	delete cont_read_from;
	delete cont;
	delete cont_r;
	delete cont_c;
}

//test Mem Bundle
int len = 100;
int i = -100;
unsigned int ui = 100;
int is[] =  {1,-2,3,-4,5,-6};
unsigned int uis[] = {1,3,4,5,6,7};
char c = 'a';
unsigned char uc = 'b';
char cs[] = {'a','b','c'};
unsigned char ucs[] = {'d','c','g'};
bool b = false;
bool bs[] = {false,true,false};
long l = -100000;
long ls[] = {1000,-300,-1200};
unsigned long ul = 10000;
unsigned long uls[] = {1000,3200,32003};
float f = -1.232;
float fs[] = {1.323,23.43,23.1};
double d = 1.23423;
double ds[] = {1322.23,4324.23,231.1};

int ri;
unsigned int rui;
int *ris;
unsigned int *ruis;
char rc;
unsigned char ruc;
char *rcs;
unsigned char *rucs;
bool rb;
bool *rbs;
long rl;
long *rls;
unsigned long rul;
unsigned long *ruls;
float rf;
float *rfs;
double rd;
double *rds;

int ris_len;
int ruis_len;
int rcs_len;
int rucs_len;
int rbs_len;
int rls_len;
int ruls_len;
int rfs_len;
int rds_len;

void clearRLen()
{
	ris_len = 0;
	ruis_len = 0;
	rcs_len = 0;
	rucs_len = 0;
	rbs_len = 0;
	rls_len = 0;
	ruls_len = 0;
	rfs_len = 0;
	rds_len = 0;
}

void delTestData()
{
	delete[] ris;
	delete[] ruis;
	delete[] rcs;
	delete[] rucs;
	delete[] rbs;
	delete[] rls;
	delete[] ruls;
	delete[] rfs;
	delete[] rds;
}

class PackageTest: public ::testing::Test 
{
public:
	PackageTest()
	{
		encap_buffer_ = new MemBuffer();
		parse_buffer_ = new MemBuffer();
		encap_bundle_ = new Bundle(*encap_buffer_);
		parse_bundle_ = new Bundle(*parse_buffer_);
	}

	~PackageTest()
	{
		delTestData();
		delete encap_bundle_;
		delete parse_bundle_;
		delete encap_buffer_;
		delete parse_buffer_;
	}
protected:
	virtual void setUp(){}
	IBuffer *encap_buffer_;
	IBuffer *parse_buffer_;
	IBundle *encap_bundle_;
	IBundle *parse_bundle_;

};

TEST_F(PackageTest,PackageCalSize)
{
	encap_buffer_->recycle();
	size_t size = 0;
	//int
	encap_bundle_->writeInt(0);
	size += encap_bundle_->calInt();
	EXPECT_EQ(size,encap_buffer_->getSize());
	int ints[2] = {1,2};
	encap_bundle_->writeInts(ints,2);
	size += encap_bundle_->calInts(2);
	EXPECT_EQ(size,encap_buffer_->getSize());
    //char
	encap_bundle_->writeChar('a');
	size += encap_bundle_->calChar();
	EXPECT_EQ(size,encap_buffer_->getSize());
	char chars[2] = {'b','c'};
	encap_bundle_->writeChars(chars,2);
	size += encap_bundle_->calChars(2);
	EXPECT_EQ(size,encap_buffer_->getSize());
    //unsigned char
    encap_bundle_->writeUChar(1);
    size += encap_bundle_->calUChar();
    EXPECT_EQ(size,encap_buffer_->getSize());
    unsigned char uchars[2] = {'b','c'};
    encap_bundle_->writeUChars(uchars,2);
    size += encap_bundle_->calUChars(2);
    EXPECT_EQ(size,encap_buffer_->getSize());
    //long
    encap_bundle_->writeLong(1);
    size += encap_bundle_->calLong();
    EXPECT_EQ(size,encap_buffer_->getSize());
    long longs[2] = {2,3};
    encap_bundle_->writeLongs(longs,2);
    size += encap_bundle_->calLongs(2);
    EXPECT_EQ(size,encap_buffer_->getSize());
    //unsigned long
    encap_bundle_->writeULong(1);
    size += encap_bundle_->calULong();
    EXPECT_EQ(size,encap_buffer_->getSize());
    unsigned long ulongs[2] = {1,2};
    encap_bundle_->writeULongs(ulongs,2);
    size += encap_bundle_->calULongs(2);
    EXPECT_EQ(size,encap_buffer_->getSize());
    //unsigned int
    encap_bundle_->writeUInt(1);
    size += encap_bundle_->calUInt();
    EXPECT_EQ(size,encap_buffer_->getSize());
    unsigned int uints[2] = {1,2};
    encap_bundle_->writeUInts(uints,2);
    size += encap_bundle_->calUInts(2);
    EXPECT_EQ(size,encap_buffer_->getSize());
    //float
    encap_bundle_->writeFloat(1.0f);
    size += encap_bundle_->calFloat();
    EXPECT_EQ(size,encap_buffer_->getSize());
    float floats[2] = {1.f,2.f};
    encap_bundle_->writeFloats(floats,2);
    size += encap_bundle_->calFloats(2);
    EXPECT_EQ(size,encap_buffer_->getSize());
    //double
    encap_bundle_->writeDouble(1.0);
    size += encap_bundle_->calDouble();
    EXPECT_EQ(size,encap_buffer_->getSize());
    double doubles[2] = {1.,2.};
    encap_bundle_->writeDoubles(doubles,2);
    size += encap_bundle_->calDoubles(2);
    EXPECT_EQ(size,encap_buffer_->getSize());
    //bool
    encap_bundle_->writeBool(true);
    size += encap_bundle_->calBool();
    EXPECT_EQ(size,encap_buffer_->getSize());
    bool bools[2] = {true,false};
    encap_bundle_->writeBools(bools,2);
    size += encap_bundle_->calBools(2);
    EXPECT_EQ(size,encap_buffer_->getSize());
}


TEST_F(PackageTest,PackageEncapAndParse)
{
	encap_buffer_->clear();
	parse_buffer_->clear();

	encap_bundle_->writeInt(i);
	encap_bundle_->writeInts(is,sizeof(is)/sizeof(int));
	encap_bundle_->writeUInt(ui);
	encap_bundle_->writeUInts(uis,sizeof(uis)/sizeof(unsigned int));
	encap_bundle_->writeChar(c);
	encap_bundle_->writeChars(cs,sizeof(cs)/sizeof(char));
	encap_bundle_->writeUChar(uc);
	encap_bundle_->writeUChars(ucs,sizeof(ucs)/sizeof(unsigned char));
	encap_bundle_->writeBool(b);
	encap_bundle_->writeBools(bs,sizeof(bs)/sizeof(bool));
	encap_bundle_->writeLong(l);
	encap_bundle_->writeLongs(ls,sizeof(ls)/sizeof(unsigned long));
	encap_bundle_->writeULong(ul);
	encap_bundle_->writeULongs(uls,sizeof(uls)/sizeof(unsigned long));
	encap_bundle_->writeFloat(f);
	encap_bundle_->writeFloats(fs,sizeof(fs)/sizeof(float));
	encap_bundle_->writeDouble(d);
	encap_bundle_->writeDoubles(ds,sizeof(ds)/sizeof(double));

	size_t size = encap_buffer_->getSize();
	const unsigned char* mem = encap_buffer_->getConstData();
	EXPECT_TRUE(mem!=NULL);
	EXPECT_TRUE(size>0);
	parse_buffer_->copy(*encap_buffer_);
	int ret = 0;//parse_bundle_->write(mem,size);
	//parse_bundle_->copy(*encap_bundle_);
	//EXPECT_EQ(ret,size);

	int ri;
	unsigned int rui;
	int *ris;
	unsigned int *ruis;
	char rc;
	unsigned char ruc;
	char *rcs;
	unsigned char *rucs;
	bool rb;
	bool *rbs;
	long rl;
	long *rls;
	unsigned long rul;
	unsigned long *ruls;
	float rf;
	float *rfs;
	double rd;
	double *rds;

	ret = -1;
	size_t rlen = 0;

	ret = parse_bundle_->readInt(ri);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(ri,i);
	ret = parse_bundle_->readInts(&ris,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(is)/sizeof(int));
	EXPECT_EQ(compareArr(ris,is,rlen),0);

	ret = parse_bundle_->readUInt(rui);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rui,ui);
	ret = parse_bundle_->readUInts(&ruis,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(uis)/sizeof(unsigned int));
	EXPECT_EQ(compareArr(ruis,uis,0),0);

	ret = parse_bundle_->readChar(rc);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rc,c);
	ret = parse_bundle_->readChars(&rcs,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(cs)/sizeof(char));
	EXPECT_EQ(compareArr(rcs,cs,rlen),0);

	ret = parse_bundle_->readUChar(ruc);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(ruc,uc);
	ret = parse_bundle_->readUChars(&rucs,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(ucs)/sizeof(unsigned char));
	EXPECT_EQ(compareArr(rucs,ucs,rlen),0);

	ret = parse_bundle_->readBool(rb);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rb,b);
	ret = parse_bundle_->readBools(&rbs,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(bs)/sizeof(bool));
	EXPECT_EQ(compareArr(rbs,bs,rlen),0);

	ret = parse_bundle_->readLong(rl);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rl,l);
	ret = parse_bundle_->readLongs(&rls,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(ls)/sizeof(long));
	EXPECT_EQ(compareArr(rls,ls,rlen),0);

	ret = parse_bundle_->readULong(rul);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rul,ul);
	ret = parse_bundle_->readULongs(&ruls,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(uls)/sizeof(unsigned long));
	EXPECT_EQ(compareArr(ruls,uls,rlen),0);

	ret = parse_bundle_->readFloat(rf);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rf,f);

	ret = parse_bundle_->readFloats(&rfs,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(fs)/sizeof(float));
	EXPECT_EQ(compareArr(rfs,fs,rlen),0);

	ret = parse_bundle_->readDouble(rd);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rd,d);
	ret = parse_bundle_->readDoubles(&rds,rlen);
	EXPECT_EQ(ret,0);
	EXPECT_EQ(rlen,sizeof(ds)/sizeof(double));
	EXPECT_EQ(compareArr(rds,ds,rlen),0);
}

TEST(CompareArrTest,compareArr)
{
	int test1[] = {1,2,3};
	int test2[] = {1,2,3};
	EXPECT_EQ(compareArr(test2,test1,3),0); 
	char test3[] = {'a','b','c'};
	char test4[] = {'c','d','f'};
	EXPECT_EQ(compareArr(test3,test4,3),-1);
}

}

using namespace utopia;
int main(int argc,char** argv)
{
	::testing::InitGoogleTest(&argc,argv);
	return RUN_ALL_TESTS();
}