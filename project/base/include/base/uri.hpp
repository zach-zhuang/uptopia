#ifndef _UTOPIA_URL_HPP_
#define _UTOPIA_URL_HPP_

#include <string>
#include <base/def.hpp>

namespace utopia
{

class Uri
{
public:
	Uri():addr_("127.0.0.1"),port_(0){};
	Uri(const std::string& addr):addr_(addr),port_(0){}
	Uri(size_t port):addr_("127.0.0.1"),port_(port){}
	Uri(const std::string& addr,const size_t port):addr_(addr),port_(port){}
	~Uri(){};
	std::string getProtocol()const {return "tcp";};
	std::string getAddress()const {return addr_;};
	size_t  getPort()const {return port_;};
	void setAddress(const std::string& addr) { addr_ = addr;}
	void  setPort(size_t port) { port_ = port;};
private:
	std::string pto_;
	std::string addr_;
	size_t port_;

};

}

#endif 