//
// Created by zoson on 18-3-23.
//

#ifndef UTOPIA_ARRAY_HPP
#define UTOPIA_ARRAY_HPP

#include <cstddef>
#include <base/def.hpp>

namespace utopia
{

template <typename Dtype>
class Array
{
public:
    Array(size_t len=0);
    Array(Dtype* data,size_t len,bool copy=false);

    Array(const Array& arr);
    Array& operator=(const Array& arr);

    virtual ~Array();
    inline const Dtype& operator[](size_t n)const{return data_[n];};
    inline Dtype& operator[](size_t n){return data_[n];};

    size_t size() const {return len_;}
    size_t& size_mutable(){return len_;}
    Dtype* data() const {return data_;}
    Dtype*& data_mutable(){return data_;}

    void setData(Dtype* data,size_t len,bool copy=false);
    void copyFrom(const Dtype* data,size_t len);
    void assign(Dtype* data,size_t len);
    size_t copyTo(Dtype** data);
    size_t copyTo(Dtype* data);
    Dtype* toPtr(size_t& len);

    void resize(size_t len);

    void clear();
    void set(Dtype d);
protected:
    Dtype *data_;
    size_t len_;

//DISABLE_COPY_AND_ASSIGN(Array);

};




}

#endif //UTOPIA_ARRAY_HPP
