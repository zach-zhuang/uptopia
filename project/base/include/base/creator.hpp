//
// Created by zoson on 18-4-14.
//

#ifndef UTOPIA_CREATOR_HPP
#define UTOPIA_CREATOR_HPP

#include <string>

namespace utopia
{

template <typename T>
class ObjectCreator
{
public:
    enum Type{BIN,TXT};
    virtual int save(T* object,Type t = TXT)=0;
    virtual int save(const std::string& path,T* object,Type t = TXT)=0;
    virtual std::string serialize(T* ob,Type t = TXT)=0;
    virtual T* load(const std::string& params,Type t = TXT)=0;
    virtual void unload(T* object)=0;
};

}

#endif //UTOPIA_CREATOR_HPP
