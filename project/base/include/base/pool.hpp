//
// Created by zoson on 18-4-6.
//

#ifndef UTOPIA_POOL_HPP
#define UTOPIA_POOL_HPP

namespace utopia
{

template <typename T>
class Pool
{
public:
    Pool(size_t size);
    virtual ~Pool();
    std::shared_ptr<T> obtain();

private:


};


}

#endif //UTOPIA_POOL_HPP
