#ifndef _UTOPIA_NOTIFIER_HPP_
#define _UTOPIA_NOTIFIER_HPP_

namespace utopia
{

template<typename Listener>
class INotifier
{
public:
	virtual void addListener(Listener *lis)=0;
	virtual void rmListener(Listener *lis)=0;
	virtual void rmAllListener()=0;
	virtual void notifyAll(int id,...)=0;
	virtual void notify(Listener* lis,int id,...)=0;
};

}


#endif