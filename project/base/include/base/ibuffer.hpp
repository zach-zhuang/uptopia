#ifndef _UTOPIA_I_BUFFER_HPP_
#define _UTOPIA_I_BUFFER_HPP_

#include <base/def.hpp>

namespace utopia
{
class IBuffer
{
public:
	enum Pos
	{
		beg,cur,end
	};

	virtual const Byte* getConstData()const = 0;
	virtual Byte* getMutableData() const= 0;

	virtual int getCapacity() const= 0;
	virtual size_t getSize() const= 0;
	virtual int resize(size_t size) = 0;
	virtual void clear() = 0;
	virtual void recycle() = 0;
	virtual void copy(const IBuffer& buf) = 0;
	virtual void reserve(size_t size)=0;
	virtual void append(const IBuffer* buf) = 0;

	virtual int read(Byte* buf,size_t size) const= 0;
	virtual int write(const Byte* buf,size_t size) = 0;

	virtual size_t tellg() const= 0 ; //write
	virtual size_t tellp() const= 0 ; //read
	virtual size_t seekg(size_t offset,Pos p=Pos::beg) = 0;
	virtual size_t seekp(size_t offset,Pos p=Pos::beg) = 0;

};

}


#endif