#ifndef _I_BUNDLE_HPP_
#define _I_BUNDLE_HPP_

#include <stddef.h>

namespace utopia
{
class IBuffer;
class IPackable;
class IBundle
{
public:
	friend class IPackable;
	virtual void writeInt(int val)=0;
	virtual void writeInts(const int* vals,size_t len)=0;
	virtual void writeUInt(unsigned int val)=0;
	virtual void writeUInts(const unsigned int* vals,size_t len)=0;
	virtual void writeChar(char val)=0;
	virtual void writeChars(const char* val,size_t len)=0;
	virtual void writeUChar(unsigned char val)=0;
	virtual void writeUChars(const unsigned char* val,size_t len)=0;
	virtual void writeBool(bool val)=0;
	virtual void writeBools(const bool* val,size_t len)=0;
	virtual void writeLong(long val)=0;
	virtual void writeLongs(const long* val,size_t len)=0;
	virtual void writeULong(unsigned long val)=0;
	virtual void writeULongs(const unsigned long* val,size_t len)=0;
	virtual void writeFloat(float val)=0;
	virtual void writeFloats(const float* val,size_t len)=0;
	virtual void writeDouble(double val)=0;
	virtual void writeDoubles(const double* val,size_t len)=0;

	virtual int readInt(int &val)const =0;
	virtual int readInts(int** val,size_t &len)const=0;
	virtual int readUInt(unsigned int &val)const=0;
	virtual int readUInts(unsigned int** val,size_t &len)const=0;
	virtual int readChar(char& val)const=0;
	virtual int readChars(char** val,size_t &len)const=0;
	virtual int readUChar(unsigned char& val)const=0;
	virtual int readUChars(unsigned char** val,size_t& len)const=0;
	virtual int readBool(bool &val)const=0;
	virtual int readBools(bool** val,size_t &len)const=0;
	virtual int readLong(long &val)const=0;
	virtual int readLongs(long** val,size_t &len)const=0;
	virtual int readULong(unsigned long &val)const=0;
	virtual int readULongs(unsigned long** val,size_t& len)const=0;
	virtual int readFloat(float &val)const=0;
	virtual int readFloats(float** val,size_t& len)const=0;
	virtual int readDouble(double &val)const=0;
	virtual int readDoubles(double **val,size_t& len)const=0;

	virtual size_t calInt()const =0;
	virtual size_t calInts(size_t len)const=0;
	virtual size_t calUInt()const=0;
	virtual size_t calUInts(size_t len)const=0;
	virtual size_t calChar()const=0;
	virtual size_t calChars(size_t len)const=0;
	virtual size_t calUChar()const=0;
	virtual size_t calUChars(size_t len)const=0;
	virtual size_t calBool()const=0;
	virtual size_t calBools(size_t len)const=0;
	virtual size_t calLong()const=0;
	virtual size_t calLongs(size_t len)const=0;
	virtual size_t calULong()const=0;
	virtual size_t calULongs(size_t len)const=0;
	virtual size_t calFloat()const=0;
	virtual size_t calFloats(size_t len)const=0;
	virtual size_t calDouble()const=0;
	virtual size_t calDoubles(size_t len)const=0;

	virtual IBuffer& getBuffer()=0;
	virtual void setBuffer(IBuffer& buf)=0;
public:
	static int TYPE_INT ;
	static int TYPE_UINT ;
	static int TYPE_CHAR ;
	static int TYPE_UCHAR ;
	static int TYPE_BOOL ;
	static int TYPE_LONG ;
	static int TYPE_ULONG ;
	static int TYPE_FLOAT ;
	static int TYPE_DOUBLE ;
	static int TYPE_STRING ;
	static int TYPE_PACKABLE ;

};


}



#endif