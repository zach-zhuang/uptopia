#ifndef _MEM_BUNDLE_HPP_
#define _MEM_BUNDLE_HPP_

#include <base/ibundle.hpp>
#include <base/ibuffer.hpp>
namespace utopia
{

class Bundle:public IBundle
{
public:
	Bundle(IBuffer& buf);
	virtual ~Bundle();

	virtual void writeInt(int val) ;
	virtual void writeInts(const int* vals,size_t len) ;
	virtual void writeUInt(unsigned int val) ;
	virtual void writeUInts(const unsigned int* vals,size_t len) ;
	virtual void writeChar(char val) ;
	virtual void writeChars(const char* val,size_t len) ;
	virtual void writeUChar(unsigned char val) ;
	virtual void writeUChars(const unsigned char* val,size_t len) ;
	virtual void writeBool(bool val) ;
	virtual void writeBools(const bool* val,size_t len) ;
	virtual void writeLong(long val) ;
	virtual void writeLongs(const long* val,size_t len) ;
	virtual void writeULong(unsigned long val) ;
	virtual void writeULongs(const unsigned long* val,size_t len) ;
	virtual void writeFloat(float val) ;
	virtual void writeFloats(const float* val,size_t len) ;
	virtual void writeDouble(double val) ;
	virtual void writeDoubles(const double* val,size_t len) ;

	virtual int readInt(int &val)const;
	virtual int readInts(int** val,size_t &len)const ;
	virtual int readUInt(unsigned int &val)const ;
	virtual int readUInts(unsigned int** val,size_t &len)const ;
	virtual int readChar(char& val)const ;
	virtual int readChars(char** val,size_t &len)const ;
	virtual int readUChar(unsigned char& val)const ;
	virtual int readUChars(unsigned char** val,size_t& len)const ;
	virtual int readBool(bool &val)const ;
	virtual int readBools(bool** val,size_t &len)const ;
	virtual int readLong(long &val)const ;
	virtual int readLongs(long** val,size_t &len) const;
	virtual int readULong(unsigned long &val)const ;
	virtual int readULongs(unsigned long** val,size_t& len)const ;
	virtual int readFloat(float &val)const ;
	virtual int readFloats(float** val,size_t& len)const ;
	virtual int readDouble(double &val)const ;
	virtual int readDoubles(double **val,size_t& len)const ;

	virtual size_t calInt()const;
	virtual size_t calInts(size_t len)const;
	virtual size_t calUInt()const;
	virtual size_t calUInts(size_t len)const;
	virtual size_t calChar()const;
	virtual size_t calChars(size_t len)const;
	virtual size_t calUChar()const;
	virtual size_t calUChars(size_t len)const;
	virtual size_t calBool()const;
	virtual size_t calBools(size_t len)const;
	virtual size_t calLong()const;
	virtual size_t calLongs(size_t len)const;
	virtual size_t calULong()const;
	virtual size_t calULongs(size_t len)const;
	virtual size_t calFloat()const;
	virtual size_t calFloats(size_t len)const;
	virtual size_t calDouble()const;
	virtual size_t calDoubles(size_t len)const;

	virtual IBuffer& getBuffer(){return buf_;};
	virtual void setBuffer(IBuffer& buf){buf_ = buf;};

	struct Header
	{
		int type;
		size_t size;
		size_t num;
	};
protected:
	size_t getElementSize(size_t size,size_t num=1) const;

	void writeDataToMem(Byte* data,int type,size_t size,size_t num);
	int readHeader(struct Header* header)const;
	int readDataFromMem(Byte* data,int type,size_t size,size_t num)const;
private:
	IBuffer &buf_;
};

}



#endif