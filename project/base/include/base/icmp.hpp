//
// Created by zoson on 18-2-4.
//

#ifndef UTOPIA_ICMP_HPP
#define UTOPIA_ICMP_HPP

#include <string>

namespace utopia
{

class ICompiler
{
public:
    virtual void loadFile(std::string filepath)=0;
    virtual void compile(std::string path)=0;
};


}

#endif //UTOPIA_ICMP_HPP
