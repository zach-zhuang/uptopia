#ifndef _UTOPIA_DEF_HPP_
#define _UTOPIA_DEF_HPP_

#include <memory>
#include <stddef.h>
#include <string>
#include <glog/logging.h>
namespace utopia
{

typedef unsigned char Byte;

typedef int status_t;

enum
{
    SUCC = 0,
    ERR = 1,
    FAIL = 2
};

#define TEMPLATE_ALL_TYPE(classname) template class classname<float>; \
    template class classname<double>; \
    template class classname<unsigned char>; \
    template class classname<char>; \
    template class classname<int>; \
    template class classname<unsigned int>; \
    template class classname<long>; \
    template class classname<unsigned long>; \
    template class classname<bool>;

#define DISABLE_COPY_AND_ASSIGN(classname) \
private:\
  classname(const classname&);\
  classname& operator=(const classname&)

#define DISABLE_COPY_AND_ASSIGN(classname) \
private:\
  classname(const classname&);\
  classname& operator=(const classname&);

#define OutClass(outclass,inclass) \
    (outclass *) ((char*)this - offsetof(outclass,inclass));

}

#endif