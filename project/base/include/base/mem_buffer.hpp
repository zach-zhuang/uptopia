#ifndef _UTOPIA_MEM_BUFFER_
#define _UTOPIA_MEM_BUFFER_

#include <base/ibuffer.hpp>

namespace utopia
{

class MemBuffer:public IBuffer
{
public:
	MemBuffer();
    MemBuffer(size_t);
	virtual ~MemBuffer();
	virtual const Byte* getConstData()const ;
	virtual Byte* getMutableData()const;

	virtual int getCapacity()const;
	virtual size_t getSize()const;
	virtual int resize(size_t size);
	virtual void clear();
	virtual void recycle();
	virtual void copy(const IBuffer& buf);
	virtual void reserve(size_t size);
	virtual void append(const IBuffer* buf);

	virtual int read(Byte* buf,size_t size)const;
	virtual int write(const Byte* buf,size_t size);

	virtual size_t tellg()const; //write
	virtual size_t tellp()const; //read
	virtual size_t seekg(size_t offset,Pos p= Pos::beg);
	virtual size_t seekp(size_t offset,Pos p= Pos::beg);


protected:
	Byte* mem_;
	struct MemState
	{
		size_t length;
		size_t capacity;
		size_t wptr;
		size_t rptr;
	};
	MemState curr_state_;
	MemState last_state_;

protected:
	void applyMoreMem(size_t needMem);
	void clearState(MemState& state);
	void saveState();
	void restoreState();	
};




}

#endif