#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <mutex>
#include <string>
#include <queue>
#include <condition_variable>
#include <base/def.hpp>

using namespace std;
namespace utopia
{
class Object;

class Cond
{
public:
	virtual bool change()=0;
	virtual bool isOK()=0;
};

class CountCond:public Cond
{
public:
	CountCond(size_t num):count_(num){}
	virtual bool change(){count_--;}
	virtual bool isOK()
	{
		if(count_==0)return true;
		return false;
	}
private:
	volatile size_t count_;
};

class Mutex:public mutex
{
    friend Object;
public:
    Mutex():mutex(),islock_(false),autoLock_(nullptr){}
    bool isLock(){return islock_;}
    void lock(){ mutex::lock(); islock_ = true;}
    bool try_lock(){ islock_ = mutex::try_lock();};
    void unlock(){islock_ = false;mutex::unlock();}
    inline void setFlag(bool islock){islock_ = islock;}
    inline void setAutoLock(std::unique_lock<std::mutex>* al){autoLock_ = al;}
    inline std::unique_lock<std::mutex>* getAutoLock(){return autoLock_;}
private:
    bool islock_;
    std::unique_lock<std::mutex>* autoLock_;
};

class AutoLock
{
public:
    inline AutoLock(Mutex& m):m_(m){autoLock_ = new std::unique_lock<std::mutex>(m);m_.setAutoLock(autoLock_);m_.setFlag(true);};
    inline ~AutoLock(){ m_.setFlag(false);m_.setAutoLock(nullptr); delete autoLock_;}
    inline void setAutoLock(std::unique_lock<std::mutex>* autolock){autoLock_ = autolock;}
    inline std::unique_lock<std::mutex>* getAutoLock(){return autoLock_;}
private:
    Mutex& m_;
    std::unique_lock<std::mutex>* autoLock_;
};

#define Sychronized(object) \
    utopia::AutoLock al(((dynamic_cast<utopia::Object*>(object)))->getMutex());

class Object
{
private:
	enum {OPENED,CLOSED};
public:
	Object();
	virtual ~Object();
	virtual inline bool equals(const Object& obj){ return (this==(&obj)); }
	virtual string toString(){return "Object";};
	void notify();
	void notifyAll();
	void wait(long time);
	void wait();
	void barrier_wait();
	void barrier_open();
	void barrier_close(std::shared_ptr<Cond> cond);
	void barrier_close(int num =-1);
	inline Mutex& getMutex(){return mutex_;}
private:
	std::shared_ptr<Cond> cond_;
    Mutex mutex_;
	condition_variable cv_;
	volatile int state_;
	volatile bool barrier_have_cond_;
};

}

#endif