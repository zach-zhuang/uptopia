//
// Created by zoson on 18-3-23.
//

#include <base/array.hpp>
#include <glog/logging.h>
#include <base/def.hpp>
namespace utopia
{

template <typename Dtype>
Array<Dtype>::Array(size_t len):len_(len),data_(NULL)
{
//    CHECK_GT(len,0);
    if(len>0)
    {
        data_ = new Dtype[len];
    }
}

template <typename Dtype>
Array<Dtype>::Array(Dtype *data, size_t len,bool copy):data_(NULL)
{
    CHECK(data!=NULL);
    CHECK_GT(len,0);
    if(copy == false)
    {
        assign(data,len);
    }else{
        copyFrom(data,len);
    }
}

template <typename Dtype>
Array<Dtype>::Array(const Array& arr):data_(NULL)
{
    copyFrom(arr.data(),arr.size());
}

template <typename Dtype>
Array<Dtype>& Array<Dtype>::operator=(const Array& arr)
{
    copyFrom(arr.data(),arr.size());
    return *this;
}

template <typename Dtype>
Array<Dtype>::~Array()
{
    clear();
}

template <typename Dtype>
size_t Array<Dtype>::copyTo(Dtype** data)
{
    if(len_==0||data_==NULL)return 0;
    *data = new Dtype[len_];
    memcpy(*data,data_,len_*sizeof(Dtype));
    return len_;
}

template <typename Dtype>
size_t Array<Dtype>::copyTo(Dtype* data)
{
    if(len_==0||data_==NULL)return 0;
    memcpy(data,data_,len_*sizeof(Dtype));
    return len_;
}

template <typename Dtype>
Dtype* Array<Dtype>::toPtr(size_t& len)
{
    len = len_;
    len_ = 0;
    Dtype* ret = data_;
    data_ = NULL;
    return ret;
}

template <typename Dtype>
void Array<Dtype>::assign(Dtype* data,size_t len)
{
    if(data_!=NULL)delete[] data_;
    data_ = data;
    len_ = len;
}

template <typename Dtype>
void Array<Dtype>::copyFrom(const Dtype* data,size_t len)
{
    CHECK(data!=NULL);
    CHECK_GT(len,0);
    if(data==NULL||len==0)return;
    if(len!=len_)
    {
        if(data!=NULL)delete []data_;
        data_ = new Dtype[len];
        len_ = len;
    }
    memcpy(data_,data,len_*sizeof(Dtype));
}

template <typename Dtype>
void Array<Dtype>::clear()
{
    if(data_!=NULL)delete[] data_;
    data_ = NULL;
    len_ = 0;
}

template <typename Dtype>
void Array<Dtype>::set(Dtype d)
{
    for(int i=0;i<len_;++i)
    {
        data_[i] = d;
    }
}

template <typename Dtype>
void Array<Dtype>::resize(size_t len)
{
    if(len==0)clear();
    if(len==len_)return;
    delete[] data_;
    len_ = len;
    data_ = new Dtype[len];
}

template <typename Dtype>
void Array<Dtype>::setData(Dtype* data,size_t len,bool copy)
{
    if(copy == false)
    {
        assign(data,len);
    }else{
        copyFrom(data,len);
    }
}


TEMPLATE_ALL_TYPE(Array)

}