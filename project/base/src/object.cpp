#include <base/object.hpp>
#include <chrono>
#include <thread>
#include <glog/logging.h>

namespace utopia
{

Object::Object():state_(CLOSED),barrier_have_cond_(false) {}

Object::~Object() {}

void Object::notify()
{
    if(!mutex_.isLock()) { LOG(ERROR)<<"notify(): ob didn't lock";return; }
    cv_.notify_one();
}

void Object::notifyAll()
{
    if(!mutex_.isLock()) { LOG(ERROR)<<"notifyAll(): ob didn't lock";return; }
    cv_.notify_all();
}

void Object::wait()
{
    if(!mutex_.isLock())
    { LOG(ERROR)<<"wait(): ob didn't lock";return; }
    cv_.wait(*mutex_.getAutoLock());
}

void Object::wait(long time)
{
    if(!mutex_.isLock())
    { LOG(ERROR)<<"wait(time): ob didn't lock";return; }
    mutex_.unlock();
    std::this_thread::sleep_for(std::chrono::milliseconds(time));
    mutex_.lock();
}

void Object::barrier_wait()
{
    if(!mutex_.isLock())
    { LOG(ERROR)<<"barrier_wait(): ob didn't lock";return; }
    if(barrier_have_cond_)
    {
        cond_->change();
        if(cond_->isOK())
        {
            barrier_open();
            barrier_have_cond_ = false;
        }
    }
    while(state_==CLOSED) { cv_.wait(*mutex_.getAutoLock()); }
}

void Object::barrier_open()
{
    if(!mutex_.isLock())
    { LOG(ERROR)<<"barrier_open(): ob didn't lock";return; }
    state_ = OPENED;
    cv_.notify_all();
}

void Object::barrier_close(std::shared_ptr<Cond> cond)
{
    if(!mutex_.isLock())
    { LOG(ERROR)<<"barrier_close(cond): ob didn't lock";return; }
    barrier_have_cond_ = true;
    state_ = CLOSED;
    cond_ = cond;
}

void Object::barrier_close(int num)
{
    if(!mutex_.isLock())
    { LOG(ERROR)<<"barrier_close(num): ob didn't lock";return; }
    state_ = CLOSED;
    if(num>0){
        barrier_have_cond_ = true;
        std::shared_ptr<Cond> count_cond(new CountCond(num));
        cond_ = count_cond;
    }else{
        barrier_have_cond_ = false;
    }
}

}