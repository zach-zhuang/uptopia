#include <base/mem_buffer.hpp>
#include <cstring>

namespace utopia
{

MemBuffer::MemBuffer():mem_(NULL)
{
	clearState(curr_state_);
	clearState(last_state_); 
}

MemBuffer::MemBuffer(size_t size):MemBuffer()
{
	resize(size);
}

MemBuffer::~MemBuffer()
{
	recycle();
}

const Byte* MemBuffer::getConstData()const
{
	return mem_;
}

Byte* MemBuffer::getMutableData()const
{
	return mem_;
}

int MemBuffer::getCapacity()const
{
	return curr_state_.capacity;
}

size_t MemBuffer::getSize()const
{
	return curr_state_.length;
}

int MemBuffer::resize(size_t size)
{
	if(curr_state_.capacity < size)
	{
		applyMoreMem(size - curr_state_.capacity);
	}
	clearState(curr_state_);
	curr_state_.length = size;
	return size;
}

void MemBuffer::recycle()
{
	delete[] mem_;
	mem_ = NULL;
	clearState(curr_state_);
}

void MemBuffer::clear()
{
	clearState(curr_state_);
}

void MemBuffer::copy(const IBuffer& buf)
{
	const MemBuffer& mem_buf = dynamic_cast<const MemBuffer&>(buf);
	if(curr_state_.capacity <mem_buf.curr_state_.length)
	{
		size_t diff = mem_buf.curr_state_.length - curr_state_.capacity;
		applyMoreMem(diff);
	}
	memcpy(this->mem_,mem_buf.mem_,mem_buf.curr_state_.length);
	int capacity = curr_state_.capacity;
	curr_state_ = mem_buf.curr_state_ ;
	curr_state_.capacity = capacity;
	last_state_ = curr_state_ ;
}

void MemBuffer::reserve(size_t size)
{
	return ;
}

void MemBuffer::append(const IBuffer* buf)
{
	return ;
}

int MemBuffer::read(Byte* buf,size_t size)const
{
	if(size == 0)return 0;
	if(buf==NULL)return 0;
	if(this->mem_==NULL||curr_state_.length==0)
	{
		return -1;
	}else{
		int r_size = curr_state_.length - curr_state_.rptr;
		if(r_size <=0)return -1;
		if(r_size>size)
		{
			r_size = size;
		}
		memcpy(buf,this->mem_+curr_state_.rptr,r_size);
		(const_cast<MemBuffer*>(this))->curr_state_.rptr += r_size;
		if(curr_state_.rptr==curr_state_.length)
		{
			(const_cast<MemBuffer*>(this))->clear();
		}
		return r_size;
	}
}

int MemBuffer::write(const Byte* buf,size_t size)
{
	if(buf==NULL)return -1;
	if(curr_state_.length  - curr_state_.wptr<size)
	{
		applyMoreMem(size - curr_state_.length  + curr_state_.wptr);
	}
	memcpy(this->mem_+curr_state_.wptr,buf,size);
	curr_state_.wptr += size;
	return size;
}

size_t MemBuffer::tellg() const//write
{
	return curr_state_.wptr;
}

size_t MemBuffer::tellp() const//read
{
	return curr_state_.rptr;
}

size_t MemBuffer::seekg(size_t offset,Pos p)
{
	if(curr_state_.length==0)return 0;
	if(p == Pos::beg)
	{
		if(offset>curr_state_.length)
		{
			curr_state_.wptr = curr_state_.length;
		}else{
			curr_state_.wptr = offset;
		}
	}else if(p == Pos::cur)
	{
		curr_state_.wptr += offset;
		if(curr_state_.wptr>curr_state_.length)
		{
			curr_state_.wptr = curr_state_.length;
		}
	}else if(p == Pos::end)
	{
		if(offset>curr_state_.length)
		{
			curr_state_.wptr = 0;
		}else{
			curr_state_.wptr = curr_state_.length - offset;
		}
	}
	return curr_state_.wptr;
}

size_t MemBuffer::seekp(size_t offset,Pos p)
{
	if(curr_state_.length==0)return 0;
	if(p == Pos::beg)
	{
		if(offset>curr_state_.length)
		{
			curr_state_.rptr = curr_state_.length;
		}else{
			curr_state_.rptr = offset;
		}
	}else if(p == Pos::cur)
	{
		curr_state_.rptr += offset;
		if(curr_state_.rptr>curr_state_.length)
		{
			curr_state_.rptr = curr_state_.length;
		}
	}else if(p == Pos::end)
	{
		if(offset>curr_state_.length)
		{
			curr_state_.rptr = 0;
		}else{
			curr_state_.rptr = curr_state_.length - offset;
		}
	}
	return curr_state_.rptr;
}

void MemBuffer::clearState(MemState &state)
{
	if(mem_ == NULL)
	{
		state.length = 0;
		state.capacity = 0;
	}
	state.wptr = 0;
	state.rptr = 0;
}

void MemBuffer::saveState()
{
	this->last_state_ = this->curr_state_;
}

void MemBuffer::restoreState()
{
	this->curr_state_ = this->last_state_;
}

void MemBuffer::applyMoreMem(size_t needMem)
{
	if(mem_==NULL){
		int size = 64;
		while(size<needMem)
		{
			if(size > 1024*1024)
			{
				size += 1024*1024;
			}else{
				size *= 2;
			}
		}
		this->curr_state_.capacity = size;
		this->mem_ = new Byte[size];
	}else if(needMem>(curr_state_.capacity-curr_state_.length)){
		int size = curr_state_.capacity;
		while((size - curr_state_.length)<needMem)
		{
			if(size > 1024*1024)
			{
				size += 1024*1024;
			}else{
				size *= 2;
			}
		}
		Byte* new_mem = new Byte[size];
		memcpy(new_mem,mem_,curr_state_.length);
		delete[] mem_;
		mem_ = new_mem;
		this->curr_state_.capacity = size;
	}
	curr_state_.length += needMem;
}


}