#include <base/bundle.hpp>

#define WRITEDATA(DATA,TYPE,TYPE_INTDEX) writeDataToMem((Byte*)&DATA,TYPE_INTDEX,sizeof(TYPE),1);
#define WRITEDATAS(DATA,TYPE,LEN,TYPE_INTDEX) writeDataToMem((Byte*)DATA,TYPE_INTDEX,sizeof(TYPE),LEN);
#define READDATA(VAL,TYPE) Header header;\
	if(readHeader(&header)==-1)return -1;\
	if(readDataFromMem((Byte*)&VAL,header.type,sizeof(TYPE),header.num)==-1)return -1;
#define READDATAS(VAL,TYPE,LEN) Header header;\
	if(readHeader(&header)==-1)return -1;\
	*VAL = new TYPE[header.num];\
	if(readDataFromMem((Byte*)*VAL,header.type,sizeof(TYPE),header.num)==-1)return -1;\
	len = header.num;

namespace utopia
{

Bundle::Bundle(IBuffer& buf):buf_(buf)
{}
 
Bundle::~Bundle() {}

size_t Bundle::getElementSize(size_t size, size_t num) const
{
	size_t ret = 0;
	ret += sizeof(Header);
	ret += size*num;
	return ret;
}

void Bundle::writeDataToMem(Byte* data,int type,size_t size,size_t num)
{
    CHECK(data!=NULL);
    if(data==NULL)return;
	struct Header header;
	header.type = type;
	header.size = size; 
	if(data==NULL||num<=0)
    {
        header.num = 0;
        buf_.write((Byte*)&header,sizeof(Header));
    }else{
        header.num = num;
        buf_.write((Byte*)&header,sizeof(Header));
        buf_.write(data,size*num);
    }

}

int Bundle::readHeader(struct Header* header)const
{
	int ret =buf_.read((Byte*)header,sizeof(Header));
	if(ret==0)return -1;
	if(ret>0)
	{
		if(header->num ==0)return -1;
	}
	return ret;
}

int Bundle::readDataFromMem(Byte* data,int type,size_t size,size_t num)const
{
	return buf_.read(data,size*num);
} 

void Bundle::writeInt(int val) 
{
	WRITEDATA(val,int,IBundle::TYPE_INT)
}

void Bundle::writeInts(const int* val,size_t len)
{
	WRITEDATAS(val,int,len,IBundle::TYPE_INT)
}

void Bundle::writeUInt(unsigned int val)
{
	WRITEDATA(val,unsigned int,IBundle::TYPE_UINT)
}

void Bundle::writeUInts(const unsigned int* val,size_t len)
{
	WRITEDATAS(val,unsigned int,len,IBundle::TYPE_UINT)
}

void Bundle::writeChar(char val)
{
	WRITEDATA(val,char,IBundle::TYPE_CHAR)
}

void Bundle::writeChars(const char* val,size_t len)
{
	WRITEDATAS(val,char,len,IBundle::TYPE_CHAR)
}

void Bundle::writeUChar(unsigned char val)
{
	WRITEDATA(val,unsigned char,IBundle::TYPE_UCHAR)
}

void Bundle::writeUChars(const unsigned char* val,size_t len)
{
	WRITEDATAS(val,unsigned char,len,IBundle::TYPE_UCHAR)
}

void Bundle::writeBool(bool val)
{
	WRITEDATA(val,bool,IBundle::TYPE_BOOL)
}

void Bundle::writeBools(const bool* val,size_t len)
{
	WRITEDATAS(val,bool,len,IBundle::TYPE_BOOL)
}

void Bundle::writeLong(long val)
{
	WRITEDATA(val,long,IBundle::TYPE_LONG)
}

void Bundle::writeLongs(const long* val,size_t len)
{
	WRITEDATAS(val,long,len,IBundle::TYPE_LONG)
}

void Bundle::writeULong(unsigned long val)
{
	WRITEDATA(val,unsigned long,IBundle::TYPE_ULONG)
}

void Bundle::writeULongs(const unsigned long* val,size_t len)
{
	WRITEDATAS(val,unsigned long,len,IBundle::TYPE_ULONG)
}

void Bundle::writeFloat(float val)
{
	WRITEDATA(val,float,IBundle::TYPE_FLOAT) 
}

void Bundle::writeFloats(const float* val,size_t len)
{
	WRITEDATAS(val,float,len,IBundle::TYPE_FLOAT)
}

void Bundle::writeDouble(double val)
{
	WRITEDATA(val,double,IBundle::TYPE_DOUBLE)
}

void Bundle::writeDoubles(const double* val,size_t len)
{
	WRITEDATAS(val,double,len,IBundle::TYPE_DOUBLE)
}

int Bundle::readInt(int &val)const
{
	READDATA(val,int)
	return 0;
}

int Bundle::readInts(int** val,size_t &len)const
{
	READDATAS(val,int,len)
	return 0;
} 

int Bundle::readUInt(unsigned int &val)const
{
	READDATA(val,unsigned int)
	return 0;
}

int Bundle::readUInts(unsigned int** val,size_t &len)const
{
	READDATAS(val,unsigned int,len)
	return 0;
}

int Bundle::readChar(char& val) const
{
	READDATA(val,char)
	return 0;
}

int Bundle::readChars(char** val,size_t &len) const
{
	READDATAS(val,char,len)
	return 0;
}

int Bundle::readUChar(unsigned char& val)const
{
	READDATA(val,unsigned char)
	return 0;
} 

int Bundle::readUChars(unsigned char** val,size_t& len)const
{
	READDATAS(val,unsigned char,len)
	return 0;
}

int Bundle::readBool(bool &val)const
{
	READDATA(val,bool)
	return 0;
}

int Bundle::readBools(bool** val,size_t &len)const
{
	READDATAS(val,bool,len)
	return 0;
}

int Bundle::readLong(long &val)const
{
	READDATA(val,long)
	return 0;
}

int Bundle::readLongs(long** val,size_t &len)const
{
	READDATAS(val,long,len)
	return 0;
}

int Bundle::readULong(unsigned long &val)const
{
	READDATA(val,unsigned long)
	return 0;
}

int Bundle::readULongs(unsigned long** val,size_t& len)const
{
	READDATAS(val,unsigned long,len)
	return 0;
}

int Bundle::readFloat(float &val) const
{
	READDATA(val,float)
	return 0;
}

int Bundle::readFloats(float** val,size_t& len)const
{
	READDATAS(val,float,len)
	return 0;
}

int Bundle::readDouble(double &val)const
{
	READDATA(val,double)
	return 0;
}

int Bundle::readDoubles(double **val,size_t& len)const
{
	READDATAS(val,double,len)
	return 0;
}

size_t Bundle::calInt()const
{
	return getElementSize(sizeof(int));
}
size_t Bundle::calInts(size_t len)const
{
	return getElementSize(sizeof(int),len);
}

size_t Bundle::calUInt()const
{
	return getElementSize(sizeof(unsigned int));
}

size_t Bundle::calUInts(size_t len)const
{
	return getElementSize(sizeof(unsigned int),len);
}

size_t Bundle::calChar()const
{
	return getElementSize(sizeof(char));
}

size_t Bundle::calChars(size_t len)const
{
	return getElementSize(sizeof(char),len);
}

size_t Bundle::calUChar()const
{
	return getElementSize(sizeof(unsigned char));
}

size_t Bundle::calUChars(size_t len)const
{
	return getElementSize(sizeof(unsigned char),len);
}

size_t Bundle::calBool()const
{
	return getElementSize(sizeof(bool));
}

size_t Bundle::calBools(size_t len)const
{
	return getElementSize(sizeof(bool),len);
}

size_t Bundle::calLong()const
{
	return getElementSize(sizeof(long));
}

size_t Bundle::calLongs(size_t len)const
{
	return getElementSize(sizeof(long),len);
}

size_t Bundle::calULong()const
{
	return getElementSize(sizeof(unsigned long));
}

size_t Bundle::calULongs(size_t len)const
{
	return getElementSize(sizeof(unsigned long),len);
}

size_t Bundle::calFloat()const
{
	return getElementSize(sizeof(float));
}

size_t Bundle::calFloats(size_t len)const
{
	return getElementSize(sizeof(float),len);
}

size_t Bundle::calDouble()const
{
	return getElementSize(sizeof(double));
}

size_t Bundle::calDoubles(size_t len)const
{
	return getElementSize(sizeof(double),len);
}

// int Bundle::TYPE_INT = 0x1;
// int Bundle::TYPE_UINT = 0x2;
// int Bundle::TYPE_CHAR = 0x3;
// int Bundle::TYPE_UCHAR = 0x4;
// int Bundle::TYPE_BOOL = 0x5;
// int Bundle::TYPE_LONG = 0x6;
// int Bundle::TYPE_ULONG = 0x7;
// int Bundle::TYPE_FLOAT = 0x8;
// int Bundle::TYPE_DOUBLE = 0x9;
// int Bundle::TYPE_STRING = 0xa;
// int Bundle::TYPE_PACKABLE = 0xb;

}