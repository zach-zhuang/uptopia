#include <base/ibundle.hpp>
namespace utopia
{

int IBundle::TYPE_INT ;
int IBundle::TYPE_UINT ;
int IBundle::TYPE_CHAR ;
int IBundle::TYPE_UCHAR ;
int IBundle::TYPE_BOOL ;
int IBundle::TYPE_LONG ;
int IBundle::TYPE_ULONG ;
int IBundle::TYPE_FLOAT ;
int IBundle::TYPE_DOUBLE ;
int IBundle::TYPE_STRING ;
int IBundle::TYPE_PACKABLE ;

}
