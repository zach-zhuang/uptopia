//
// Created by zoson on 18-3-24.
//

#include <rpc/test_parcel.hpp>
#include <base/bundle.hpp>

namespace utopia
{

TestParcel::TestParcel() {}

TestParcel::~TestParcel() {}

void TestParcel::writeToBuffer(IBuffer &buf)
{
    Bundle bundle(buf);
    bundle.writeInt(i_);
    bundle.writeChars(cs_.data(),cs_.size());
    bundle.writeFloats(fs_.data(),fs_.size());
}

int TestParcel::readFromBuffer(IBuffer &buf)
{
    Bundle bundle(buf);
    bundle.readInt(i_);
    bundle.readChars(&cs_.data_mutable(),cs_.size_mutable());
    bundle.readFloats(&fs_.data_mutable(),fs_.size_mutable());
}

void TestParcel::setCs(char* data,size_t len,bool copy)
{
    cs_.setData(data,len,copy);
}

void TestParcel::setFs(float* data,size_t len,bool copy)
{
    fs_.setData(data,len,copy);
}

void TestParcel::setCs(Array<char>& arr)
{
    cs_ = arr;
}

void TestParcel::setFs(Array<float>& arr)
{
    fs_ = arr;
}

}