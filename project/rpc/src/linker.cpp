//
// Created by zoson on 18-3-17.
//

#include <rpc/linker.hpp>
#include <base/mem_buffer.hpp>
#include <base/io.hpp>

namespace utopia
{

Linker::Linker(IO *io):io_(io)
{
    regFunc(LINKER_FIRST);
    regFunc(LINKER_ping);
    regFunc(LINKER_unLink);
    regFunc(LINKER_isLive);
}

Linker::~Linker()
{
    io_->close();
    std::map<int,IBuffer*>::iterator it;
    it = map_ibuf_.begin();
    while(it!=map_ibuf_.end())
    {
        delete it->second;
        it++;
    }
    it = map_obuf_.begin();
    while(it!=map_obuf_.end())
    {
        delete it->second;
        it++;
    }
}

void Linker::regFunc(int id)
{
    if(map_obuf_.find(id)==map_obuf_.end())
    {
        map_obuf_.insert(std::pair<int,IBuffer*>(id,new MemBuffer));
    }
    if(map_ibuf_.find(id)==map_ibuf_.end())
    {
        map_ibuf_.insert(std::pair<int,IBuffer*>(id,new MemBuffer));
    }
}


}