//
// Created by zoson on 18-3-17.
//

#include <rpc/linker_proxy.hpp>
#include <base/io.hpp>
#include <base/bundle.hpp>
namespace utopia
{

LinkerProxy::LinkerProxy(IO* io):Linker(io)
{}

LinkerProxy::~LinkerProxy()
{}

bool LinkerProxy::invoke(int code, utopia::IBundle *params, utopia::IBundle *reply, int flags)
{
    bool ret = false;
    int first_code = LINKER_FIRST;
    IBuffer* first_ibuf = map_ibuf_.find(first_code)->second;
    IBuffer* first_obuf = map_obuf_.find(first_code)->second;
    IBundle *first_params = new Bundle(*first_obuf);
    IBundle *first_reply = new Bundle(*first_ibuf);

    first_ibuf->clear();
    first_obuf->clear();
    first_params->writeInt(code);
    io_->writeSyn(*first_obuf);
    int req_code = -1;
    io_->readSyn(*first_ibuf);
    first_reply->readInt(req_code);
    if(req_code != code)
    {
        LOG(INFO)<<"Request code doesn't match. req_code:"<<req_code<<" code:"<<code ;
        ret = false;
    }else{
        io_->writeSyn((params->getBuffer()));
        io_->readSyn((reply->getBuffer()));
        ret = true;
    }
    return ret;
}

bool LinkerProxy::onInvoke(int code, utopia::IBundle *params, utopia::IBundle *reply, int flags)
{}

bool LinkerProxy::ping()
{
    bool ret = false;
    std::shared_ptr<IBundle> params,reply;
    CHECK(getBuffer(LINKER_ping,params,reply));
    bool ok = invoke(LINKER_ping,params.get(),reply.get(),0);
    if(!ok)return ok;
    reply->readBool(ret);
    return ret;
}

bool LinkerProxy::unLink()
{
    bool ret = false;
    std::shared_ptr<IBundle> params,reply;
    CHECK(getBuffer(LINKER_unLink,params,reply));
    bool ok = invoke(LINKER_unLink,params.get(),reply.get(),0);
    if(!ok)return ok;
    reply->readBool(ret);
    return ret;
}

bool LinkerProxy::isLive()
{
    bool ret = false;
    std::shared_ptr<IBundle> params,reply;
    CHECK(getBuffer(LINKER_isLive,params,reply));
    bool ok = invoke(LINKER_isLive,params.get(),reply.get(),0);
    if(!ok)return ok;
    reply->readBool(ret);
    return ret;
}

bool LinkerProxy::getBuffer(int code,std::shared_ptr<IBundle>& params,std::shared_ptr<IBundle>& reply)
{
    std::map<int,IBuffer*>::iterator it_params = map_obuf_.find(code);
    std::map<int,IBuffer*>::iterator it_reply = map_ibuf_.find(code);
    if(it_params==map_ibuf_.end()||it_reply==map_obuf_.end())
    {
        return false;
    }else{
        params.reset(new Bundle(*it_params->second));
        reply.reset(new Bundle(*it_reply->second));
    }
    return true;
}

}