//
// Created by zoson on 18-2-4.
//

#include <rpc/linker_stub.hpp>
#include <base/mem_buffer.hpp>
#include <base/io.hpp>
#include <base/bundle.hpp>

namespace utopia
{

LinkerStub::LinkerStub(IO* io):Linker(io)
{}

LinkerStub::~LinkerStub()
{}

bool LinkerStub::loop()
{
    //LOG(INFO)<<"loop";
    int first_code = LINKER_FIRST;
    IBuffer* first_ibuf = map_ibuf_.find(first_code)->second;
    IBuffer* first_obuf = map_obuf_.find(first_code)->second;
    std::shared_ptr<IBundle> first_params(new Bundle(*first_ibuf));
    std::shared_ptr<IBundle> first_reply(new Bundle(*first_obuf));

    while(true)
    {
        first_ibuf->clear();
        first_obuf->clear();
        int first_read_size = io_->readSyn(*first_ibuf);
        LOG(INFO)<<"first_read_size "<<first_read_size;
        int req_code = -1;
        first_params->readInt(req_code);

        std::map<int,IBuffer*>::iterator it_params = map_ibuf_.find(req_code);
        std::map<int,IBuffer*>::iterator it_reply = map_obuf_.find(req_code);
        if(it_params==map_ibuf_.end()||it_reply==map_obuf_.end())
        {
            //LOG(INFO)<<"Buffer doesn't exited req_code:"<<req_code;
            first_reply->writeInt(-1);
            io_->writeSyn(*first_obuf);
            continue;
        }else{
            //LOG(INFO)<<"Buffer exits";
            first_reply->writeInt(req_code);
            io_->writeSyn(*first_obuf);
            IBuffer* ibuf = it_params->second;
            IBuffer* obuf = it_reply->second;
            ibuf->clear();
            obuf->clear();
            IBundle *params = new Bundle(*ibuf);
            IBundle *reply = new Bundle(*obuf);

            io_->readSyn(*ibuf);
            invoke(req_code,params,reply,0);
            io_->writeSyn(*obuf);

            delete params;
            delete reply;
        }
        //break;
    }
}

bool LinkerStub::invoke(int code, utopia::IBundle *params, utopia::IBundle *reply, int flags)
{
    //LOG(INFO)<<"Invoke code "<<code;
    bool r = onInvoke(code,params,reply,0);
    return r;
}

bool LinkerStub::onInvoke(int code, utopia::IBundle *params, utopia::IBundle *reply, int flags)
{
    //LOG(INFO)<<"onInvoke"<<code;
    switch(code)
    {
        case LINKER_ping:
            LOG(INFO)<<"Linker ping";
            reply->writeBool(ping());
            return true;
            break;
        case LINKER_unLink:
            LOG(INFO)<<"Linker unlink";
            reply->writeBool(unLink());
            return true;
            break;
        case LINKER_isLive:
            LOG(INFO)<<"Linker islive";
            reply->writeBool(isLive());
            return true;
            break;
        default:
            return false;
            break;
    }
}


bool LinkerStub::ping()
{
    return true;
}

bool LinkerStub::isLive()
{
    return false;
}

bool LinkerStub::unLink()
{
    return true;
}



}