//
// Created by zoson on 18-2-11.
//

#include <rpc/tmp_linker.hpp>
#include <rpc/linker_stub.hpp>

namespace utopia
{

TemplateStub::TemplateStub(IO* io):LinkerStub(io)
{

}

ILinker* TemplateStub::asLinker()
{
    return this;
}

TemplateProxy::TemplateProxy(ILinker *remote):remote_(remote) {}

ILinker* TemplateProxy::asLinker()
{
    return remote_;
}

int TemplateProxy::opAdd(int a, int b)
{

}



}
