//
// Created by zoson on 18-3-18.
//

#include <rpc/io_formated.hpp>
#include <base/def.hpp>
#include <base/bundle.hpp>
#include <base/mem_buffer.hpp>

namespace utopia
{

IOFormated::IOFormated(IO &io):io_(io),head_buf_(new MemBuffer),head_size_(0)
{
    Bundle bundle(*head_buf_);
    head_size_ = bundle.calULong();
    //LOG(INFO)<<"head size "<<head_size_;
}

int IOFormated::readSyn(IBuffer &buf)
{
    //LOG(INFO)<<"readSyn";
    Bundle head_bundle(*head_buf_);
    head_buf_->resize(head_size_);
    int head_read = io_.readSyn(*head_buf_);
    //LOG(INFO)<<"read head size"<<head_read;
    size_t buf_size = 0;
    head_bundle.readULong(buf_size);
    //LOG(INFO)<<"buf size"<<buf_size;
    buf.resize(buf_size);
    int buf_read = io_.readSyn(buf);
    //LOG(INFO)<<"read buf size"<<buf_read;
    return buf_read;
}

int IOFormated::writeSyn(const IBuffer& buf)
{
    //LOG(INFO)<<"writeSyn";
    size_t buf_size = buf.getSize();
    head_buf_->resize(head_size_);
    Bundle bundle(*head_buf_);
    bundle.writeULong(buf_size);
    //LOG(INFO)<<"write head size"<<head_buf_->getSize();
    int head_write = io_.writeSyn(*head_buf_);
    //LOG(INFO)<<"write head write"<<head_write;
    int buf_write = io_.writeSyn(buf);
    //LOG(INFO)<<"write buf write"<<buf_write;
    return buf_write;
}

void IOFormated::readASyn(IBuffer *buf)
{
    //LOG(INFO)<<"readASyn";
}

void IOFormated::writeASyn(const IBuffer *buf)
{
    //LOG(INFO)<<"writeASyn";
}

void IOFormated::close()
{
    io_.close();
}


}