//
// Created by zoson on 18-3-18.
//

#ifndef UTOPIA_IO_HELPER_HPP
#define UTOPIA_IO_HELPER_HPP

#include <base/io.hpp>

namespace utopia
{
class IOFormated:public IO
{
public:
    IOFormated(IO& io);
    virtual int openSyn(const Uri& uri){}
    virtual int readSyn(IBuffer &buf);
    virtual int writeSyn(const IBuffer &buf);
    virtual void readASyn(IBuffer *buf);
    virtual void writeASyn(const IBuffer *buf);
    virtual void close();
    virtual void run(){}
    virtual int getState(){return io_.getState();}
protected:
    virtual int open(const Uri& uri){};
private:
    IO &io_;
    IBuffer *head_buf_;
    size_t head_size_;
};


}

#endif //UTOPIA_IO_HELPER_HPP
