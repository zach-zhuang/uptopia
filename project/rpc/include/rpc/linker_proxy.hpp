//
// Created by zoson on 18-3-17.
//

#ifndef UTOPIA_LINKER_PROXY_HPP
#define UTOPIA_LINKER_PROXY_HPP

#include <rpc/linker.hpp>
#include <memory>

namespace utopia
{

class LinkerProxy:public Linker
{
public:
    LinkerProxy(IO* io);
    virtual ~LinkerProxy();
    virtual bool invoke(int code,IBundle* params,IBundle* ret,int flags);
    virtual bool onInvoke(int code,IBundle* params,IBundle* ret,int flags);
    virtual bool ping();
    virtual bool isLive();
    virtual bool unLink();
    virtual bool isProxy(){return true;};
protected:
    bool getBuffer(int code,std::shared_ptr<IBundle>& params,std::shared_ptr<IBundle>& reply);
};

}

#endif //UTOPIA_LINKER_PROXY_HPP
