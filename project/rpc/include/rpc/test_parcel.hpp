//
// Created by zoson on 18-3-23.
//

#ifndef UTOPIA_TEST_PARCEL_HPP
#define UTOPIA_TEST_PARCEL_HPP

#include <rpc/iparcel.hpp>
#include <base/array.hpp>
namespace utopia
{

class TestParcel:public IParcel
{
public:
    TestParcel();
    virtual ~TestParcel();
    virtual void writeToBuffer(IBuffer& buf);
    virtual int readFromBuffer(IBuffer& buf);

    int getI() const {return i_;}
    Array<char>& getCs() {return cs_;}
    Array<float>& getFs() {return fs_;}

    void setI(int i){ i_ = i;}
    void setCs(Array<char>& arr);
    void setFs(Array<float>& arr);
    void setCs(char* data,size_t len,bool copy = false);
    void setFs(float* data,size_t len,bool copy = false);
protected:
    int i_;
    Array<char> cs_;
    Array<float> fs_;
};


}

#endif //UTOPIA_PARCEL_HPP
