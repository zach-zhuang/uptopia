//
// Created by zoson on 18-4-14.
//

#ifndef UTOPIA_ILINKERCTX_HPP
#define UTOPIA_ILINKERCTX_HPP

#include <rpc/ilinker.hpp>

namespace utopia
{

class IBuffer;
class ILinker;

class ILinkerCtx
{
public:
    virtual ILinker* load(const std::string& params)=0;
    virtual void unload(ILinker* linker)=0;
    virtual int handup(ILinker* linker,IBuffer* ibuf,IBuffer* obuf)=0;
};

}

#endif //UTOPIA_ILINKERCTX_HPP
