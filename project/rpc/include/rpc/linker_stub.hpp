//
// Created by zoson on 18-2-4.
//

#ifndef UTOPIA_LINKER_STUB_HPP
#define UTOPIA_LINKER_STUB_HPP

#include <rpc/linker.hpp>

namespace utopia
{

class LinkerStub:public Linker
{
public:
    LinkerStub(IO* io);
    virtual ~LinkerStub();
    virtual bool loop();
    virtual bool invoke(int code,IBundle* params,IBundle* ret,int flags);
    virtual bool onInvoke(int code,IBundle* params,IBundle* ret,int flags);
    virtual bool ping();
    virtual bool isLive();
    virtual bool unLink();
    virtual bool isProxy(){return false;};
protected:
};

}

#endif //UTOPIA_LINKER_STUB_HPP
