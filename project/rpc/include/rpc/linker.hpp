//
// Created by zoson on 18-3-17.
//

#ifndef UTOPIA_LINKER_HPP
#define UTOPIA_LINKER_HPP

#include <rpc/ilinker.hpp>
#include <map>
namespace utopia
{

class IO;
class IBuffer;

class Linker:public ILinker
{
public:
    Linker(IO* io);
    virtual ~Linker();
    virtual bool loop(){ return false; };
    virtual bool invoke(int code,IBundle* params,IBundle* ret,int flags)=0;
    virtual bool onInvoke(int code,IBundle* params,IBundle* ret,int flags)=0;
    virtual bool ping()=0;
    virtual bool isLive()=0;
    virtual bool unLink()=0;
    virtual bool isProxy()=0;

protected:
    void regFunc(int id);

protected:
    IO* io_;
    std::map<int,IBuffer*> map_ibuf_;
    std::map<int,IBuffer*> map_obuf_;

    static const int LINKER_FIRST = 0;
    static const int LINKER_ping = 1;
    static const int LINKER_unLink = 2;
    static const int LINKER_isLive = 3;
};

}

#endif //UTOPIA_LINKER_HPP
