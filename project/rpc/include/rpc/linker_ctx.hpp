//
// Created by zoson on 18-2-13.
//

#ifndef UTOPIA_LNKER_CTX_HPP
#define UTOPIA_LNKER_CTX_HPP

#include <string>
#include <rpc/ilinkerCtx.hpp>
namespace utopia
{


class LinkerCtx:public ILinkerCtx
{
public:
    LinkerCtx();
    LinkerCtx(ILinkerCtx* parent);
    virtual ~LinkerCtx();
    virtual int handUp(ILinker*linker,IBuffer* ibuf,IBuffer* obuf);
protected:
    ILinkerCtx *parent_;
};

}

#endif //UTOPIA_ILNKER_CTX_HPP
