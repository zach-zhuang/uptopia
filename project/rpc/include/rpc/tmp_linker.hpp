//
// Created by zoson on 18-2-11.
//

#ifndef UTOPIA_TMP_LINKER_HPP
#define UTOPIA_TMP_LINKER_HPP

#include <rpc/iapi.hpp>
#include <rpc/linker_stub.hpp>

namespace utopia
{

class Template:public IApi
{
public:
    virtual int opAdd(int a,int b)=0;

protected:
    static const int LINKER_opAdd = 4;

};

class TemplateStub:public LinkerStub,public Template
{
public:
    TemplateStub(IO* io);
    ILinker* asLinker();
    bool onInvoke(int code, utopia::IBundle *params, utopia::IBundle *ret, int flags);
};

class TemplateProxy:public Template
{
public:
    TemplateProxy(ILinker *remote);
    ILinker* asLinker();
    bool onInvoke(int code, utopia::IBundle *params, utopia::IBundle *ret, int flags);
    int opAdd(int a,int b);
private:
    ILinker *remote_;
};

}

#endif //UTOPIA_TMP_LINKER_HPP
