//
// Created by zoson on 18-3-23.
//

#ifndef UTOPIA_IPARCEL_HPP
#define UTOPIA_IPARCEL_HPP

namespace utopia
{

class IBuffer;

class IParcel
{
public:
    virtual void writeToBuffer(IBuffer& buf)=0;
    virtual int readFromBuffer(IBuffer& buf)=0;
};


}

#endif //UTOPIA_IPARCEL_HPP
