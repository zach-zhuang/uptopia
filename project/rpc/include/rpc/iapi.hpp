//
// Created by zoson on 18-2-11.
//

#ifndef UTOPIA_IAPI_HPP
#define UTOPIA_IAPI_HPP

namespace utopia
{

class ILinker;

class IApi
{
public:
    virtual ILinker* asLinker() = 0;
};

}

#endif //UTOPIA_IAPI_HPP
