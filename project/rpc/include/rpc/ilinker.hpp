#ifndef _UTOPIA_ILINKER_HPP_
#define _UTOPIA_ILINKER_HPP_

#include <base/def.hpp>

namespace utopia
{

class IBundle;

class IBaseLinker
{
public:

};

class ILinker
{
public:
    virtual bool invoke(int code,IBundle* params,IBundle* ret,int flags)=0;
    virtual bool onInvoke(int code,IBundle* params,IBundle* ret,int flags)=0;
    virtual bool ping()=0;
    virtual bool isLive()=0;
    virtual bool unLink()=0;
    virtual bool isProxy()=0;
};

}

#endif