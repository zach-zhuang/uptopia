//
// Created by zoson on 18-4-7.
//
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <io/socket.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
#include <rpc/linker_stub.hpp>
#include <rpc/io_formated.hpp>
#include "../../../../workspace/utopia/proc/include/proc/process.hpp"
#include <thread/thread.hpp>
#include "../../../../workspace/utopia/proc/include/proc/multi_proc.hpp"
#include <rpc/linker_proxy.hpp>

using namespace boost::asio;
using namespace boost::posix_time;
using namespace utopia;
class ProcessStub:public Process
{
public:
    virtual int main(const std::vector<std::string>& args)
    {
        AcceptThread *at = new AcceptThread;
        HandleThread *ht = new HandleThread;
        at->start();
        ht->start();
        at->join();
        ht->join();
        return 0;
    }
    class AcceptThread:public Thread
    {
    public:
        virtual void run()
        {
            accept_thread(this);
        }
    };
    class HandleThread:public Thread
    {
    public:
        virtual void run()
        {
            handle_clients_thread(this);
        }
    };
static void accept_thread(Thread* t) {
    try{
        server.bind(utopia::Uri());
        while ( true) {
            io = server.acceptIO();
            iof = new IOFormated(*io);
            stub = new LinkerStub(iof);
        }
    }catch (boost::system::system_error & err){
        LOG(ERROR)<<err.what();
    }
}

static void handle_clients_thread(Thread* t) {
    try{
        while ( true)
        {
            boost::this_thread::sleep( millisec(1));
            if(io==NULL)continue;
            stub->loop();
        }
    }catch (boost::system::system_error & err){
        LOG(ERROR)<<"ProcessStub handle_clients_thread "<<err.what();
    }
}

static SocketServer server;
static boost::recursive_mutex cs;
static IO* io  ;
static LinkerStub *stub;
static IOFormated* iof;

};

SocketServer ProcessStub::server;
boost::recursive_mutex ProcessStub::cs;
IO* ProcessStub::io = NULL;
LinkerStub * ProcessStub::stub;
IOFormated* ProcessStub::iof;

class ProcessProxy:public Process
{
public:
    ProcessProxy(){}
    virtual int main(const std::vector<std::string>& args)
    {
        SocketClient *client = new SocketClient(utopia::Uri());
        IOFormated iof(*client);
        LinkerProxy proxy(&iof);

        try {
            while ( true) {
                LOG(INFO)<<"ProcessProxy while true";
                bool ret = proxy.ping();
                LOG(INFO)<<"ProcessProxy ping "<<ret;
                ret = false;
                ret = proxy.isLive();
                LOG(INFO)<<"ProcessProxy isLive "<<ret;
                ret = false;
                ret = proxy.unLink();
                LOG(INFO)<<"ProcessProxy unLink "<<ret;
                //std::cout<<"speed "<<total/(clock()-time)*CLOCKS_PER_SEC<<" mb/s"<<std::endl;
                boost::this_thread::sleep(boost::posix_time::millisec(1000));
                //break;
            }
        } catch(boost::system::system_error & err) {
            LOG(ERROR) << "client terminated " <<err.what();
        }
        return 0;
    }
};

int main(int argc,char**argv)
{
    google::InitGoogleLogging(argv[0]);
    google::SetStderrLogging(google::GLOG_INFO);
    MultiProc proc;
    std::shared_ptr<Process> p1(new ProcessStub);
    proc.create(p1);
    sleep(2);
    std::shared_ptr<Process> p2(new ProcessProxy);
    proc.create(p2);
    proc.waitAll();
    return 0;
}
