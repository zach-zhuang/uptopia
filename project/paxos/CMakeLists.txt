cmake_minimum_required(VERSION 2.8.7)
project(paxos)

# -g2 for debug -O2 for production
set(CMAKE_CXX_FLAGS "-fPIC -std=c++14 -g2 -Wall -m64 -Wno-unused-local-typedefs ")
#set(CMAKE_LINK_LIBRARY_FLAG "-Wl --no-as-needed")

set(LINK_DEP_LIBS)
set(LINK_SRC_LIBS)

include_directories(./include)
include_directories(./plugin/include)

include(../../cmake/utils.cmake)

IMPORT(../../project/io utopia_io)
list(APPEND LINK_DEP_LIBS utopia_io)

IMPORT(../../project/thread utopia_thread)
list(APPEND LINK_DEP_LIBS utopia_thread)

IMPORT(../../project/base utopia_base)
list(APPEND LINK_DEP_LIBS utopia_base)

# third party
set(third_party ${PROJECT_SOURCE_DIR}/../../3rdparty)

include(../../cmake/boost.cmake)
FIND_BOOST(BOOST_BOTH_LIBRARIES)
message(STATUS ${BOOST_BOTH_LIBRARIES})
list(APPEND LINK_DEP_LIBS ${BOOST_BOTH_LIBRARIES})

# protobuf
set(protobuf ${third_party}/protobuf)
include_directories(${protobuf}/include)
link_directories(${protobuf}/lib)
list(APPEND LINK_DEP_LIBS ${protobuf}/lib/libprotobuf.a)
set(protoc ${protobuf}/bin/protoc)

# glog
set(glog ${third_party}/glog)
set(glog ${third_party}/glog)
include_directories(${glog}/include)
link_directories(${glog}/lib)
list(APPEND LINK_DEP_LIBS ${glog}/lib/libglog.a)

# gflags
set(gflags ${third_party}/gflags)
include_directories(${gflags}/include)
link_directories(${gflags}/lib)
list(APPEND LINK_DEP_LIBS ${gflags}/lib/libgflags.a)

# leveldb
set(leveldb ${third_party}/leveldb)
include_directories(${leveldb}/include)
link_directories(${leveldb}/lib)
list(APPEND LINK_DEP_LIBS ${leveldb}/lib/libleveldb.a)
list(APPEND LINK_DEP_LIBS snappy)

# gmock
set(gtest ${third_party}/googletest)
set(gmock ${gtest}/googlemock)
include_directories(${gmock}/include)
include_directories(${gmock})
include_directories(${gtest}/googletest/include)
include_directories(${gtest}/googletest)
add_library(gtest-mock SHARED ${gtest}/googletest/src/gtest-all.cc ${gmock}/src/gmock-all.cc)
#link_directories(${gmock}/lib)
#list(APPEND LINK_DEP_LIBS ${gmock}/lib/libgmock.a)

# grpc
set(grpc ${third_party}/grpc)
include_directories(${grpc}/include)
link_directories(${grpc}/lib)
list(APPEND LINK_DEP_LIBS ${third_party}/grpc/libgrpc++_unsecure.a
        ${third_party}/grpc/libgrpc.a
        ${third_party}/grpc/libgpr.a
        ${third_party}/grpc/libaddress_sorting.a
        ${third_party}/grpc/lib/libcares.a
        ${third_party}/grpc/third_party/boringssl/ssl/libssl.a
        ${third_party}/grpc/third_party/boringssl/crypto/libcrypto.a
        ${third_party}/grpc/lib/libz.a)

list(APPEND LINK_DEP_LIBS pthread )

set(PROTO_HDRS)
# dep libs
function(PROTOC_CPP PROTOC FILE SRCS HDRS)
    get_filename_component(DIR ${FILE} DIRECTORY)
    get_filename_component(FIL ${FILE} NAME_WE)
    set(SRCS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.cc")
    set(HDRS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.h")
    add_custom_command(
            OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.cc"
            "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.h"
            COMMAND  ${PROTOC}
            ARGS -I ${DIR} --cpp_out  ${CMAKE_CURRENT_BINARY_DIR} ${FILE}
            DEPENDS ${FILE} ${PROTOC}
            COMMENT "Running C++ protocol buffer compiler on ${FIL}.proto"
            VERBATIM )
    set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
    set(${SRCS} ${SRCS_T} PARENT_SCOPE)
    set(${HDRS} ${HDRS_T} PARENT_SCOPE)
    list(APPEND ${PROTO_HDRS} ${HDRS} PARENT_SCOPE)
endfunction()

function(PROTOC_CPP_GRPC PROTOC FILE SRCS HDRS)
    get_filename_component(DIR ${FILE} DIRECTORY)
    get_filename_component(FIL ${FILE} NAME_WE)
    set(SRCS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.cc")
    set(HDRS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.h")
    add_custom_command(
            OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.cc"
            "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.h"
            COMMAND  ${PROTOC}
            ARGS -I ${DIR} --grpc_out=${CMAKE_CURRENT_BINARY_DIR} --plugin=protoc-gen-grpc=${third_party}/grpc/grpc_cpp_plugin ${FILE}
            DEPENDS ${FILE} ${PROTOC}
            COMMENT "Running C++ protocol buffer compiler on ${FIL}.proto grpc"
            VERBATIM )
    set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
    set(${SRCS} ${SRCS_T} PARENT_SCOPE)
    set(${HDRS} ${HDRS_T} PARENT_SCOPE)
    list(APPEND ${PROTO_HDRS} ${HDRS} PARENT_SCOPE)
endfunction()

function(PROTOC_LIBS PROTOC PATH PROTO_LIBS PROTO_FILES)
    file(GLOB_RECURSE PATH_ALL_PROTO "${PATH}/*.proto")
    set(PROTO_LIBS_T)
    set(PROTO_FILES_T)
    foreach(PROTO ${PATH_ALL_PROTO})
        get_filename_component(FIL ${PROTO} NAME_WE)
        PROTOC_CPP(${PROTOC} ${PROTO} SRCS HDRS)
        add_library(${FIL}_proto ${SRCS} ${HDRS})
        list(APPEND PROTO_LIBS_T ${FIL}_proto)
        list(APPEND PROTO_FILES_T ${HDRS})
    endforeach()
    set(${PROTO_LIBS} ${PROTO_LIBS_T} PARENT_SCOPE)
    set(${PROTO_FILES} ${PROTO_FILES_T} PARENT_SCOPE)
endfunction()
include_directories(${CMAKE_BINARY_DIR})
PROTOC_LIBS(${protoc} ${PROJECT_SOURCE_DIR}/src PROTO_LIBS PROTO_FILES)

macro(SUBDIRLIST result curdir)
    file(GLOB children RELATIVE ${curdir} ${curdir}/*)
    foreach(child ${children})
        if(IS_DIRECTORY ${curdir}/${child})
            list(APPEND dirlist ${child})
        endif()
    endforeach()
    set(${result} ${dirlist})
endmacro()

function(RECINCLUDE curdir)
    file(GLOB children RELATIVE ${curdir} ${curdir}/*)
    include_directories(${curdir})
    foreach(child ${children})
        if(IS_DIRECTORY ${curdir}/${child})
            include_directories(${curdir}/${child})
            RECINCLUDE(${curdir}/${child})
        endif()
    endforeach()
endfunction()

function(SRC_LIBS LINK_SRC_LIBS SRC_DIR SUB_DIRS)
    set(SRC_LIBS)
    set(INDEX 2)
    set(LAST ${ARGC})
    while(INDEX LESS ${LAST})
        set(DIR ${ARGV${INDEX}})
        RECINCLUDE(${SRC_DIR}/${DIR})
        if(NOT ("${DIR}" MATCHES "include") )
            file(GLOB_RECURSE HPP_FILES "${SRC_DIR}/${DIR}/*.h")
            file(GLOB_RECURSE CPP_FILES "${SRC_DIR}/${DIR}/*.cpp")
            include_directories(${SRC_DIR}/${DIR})
            add_library(${DIR}o SHARED STATIC ${CPP_FILES} ${HPP_FILES} ${PROTO_FILES})
            list(APPEND SRC_LIBS ${DIR}o)
        endif()
        math(EXPR INDEX "${INDEX} + 1")
    endwhile()
    set(${LINK_SRC_LIBS} ${SRC_LIBS} PARENT_SCOPE)
endfunction()

list(APPEND SRC_SUB_DIR benchmark node master algorithm communicate comm config logstorage sm-base tools utils checkpoint test)
SRC_LIBS(LINK_SRC_LIBS ${PROJECT_SOURCE_DIR}/src ${SRC_SUB_DIR} )

list(APPEND PLUGIN_SUB_DIR monitor logger_google)
SRC_LIBS(LINK_PLUGIN_LIBS ${PROJECT_SOURCE_DIR}/plugin ${PLUGIN_SUB_DIR} )

file(GLOB_RECURSE SAMPLE_PHXECHO_CPP_FILES "sample/phxecho/*.cpp")
add_executable(phxecho ${SAMPLE_PHXECHO_CPP_FILES} utopia_io utopia_base utopia_thread)
target_link_libraries(phxecho  ${LINK_SRC_LIBS} ${LINK_PLUGIN_LIBS} ${PROTO_LIBS} ${LINK_DEP_LIBS}   )

file(GLOB_RECURSE SAMPLE_PHXELE_CPP_FILES "sample/phxelection/*.cpp")
add_executable(phxele ${SAMPLE_PHXELE_CPP_FILES})
add_custom_target(${phxele} ALL DEPENDS ${PROTO_LIBS})
target_link_libraries(phxele ${LINK_SRC_LIBS} ${LINK_PLUGIN_LIBS} ${PROTO_LIBS} ${LINK_DEP_LIBS} )

PROTOC_CPP_GRPC(${protoc} ${PROJECT_SOURCE_DIR}/sample/phxkv/phxkv.proto PHXKV_SRCS_GRPC PHXKV_HDRS_GRPC)
add_library(phxkv_proto_grpc ${PHXKV_SRCS_GRPC} ${PHXKV_HDRS_GRPC})
PROTOC_CPP(${protoc} ${PROJECT_SOURCE_DIR}/sample/phxkv/phxkv.proto PHXKV_SRCS PHXKV_HDRS)
message(STATUS ${PHXKV_SRCS})
add_library(phxkv_proto ${PHXKV_SRCS} ${PHXKV_HDRS})

include_directories(./sample/phxkv)
file(GLOB_RECURSE SAMPLE_PHXKV_CPP_FILES "sample/phxkv/*.cpp")
set(PHXKV_SRC ${PROJECT_SOURCE_DIR}/sample/phxkv)
add_executable(phxkv_server ${PHXKV_SRC}/kv.cpp
                     ${PHXKV_SRC}/def.h
                     ${PHXKV_SRC}/kv.h
                     ${PHXKV_SRC}/kv_grpc_server.cpp
                     ${PHXKV_SRC}/kv_grpc_server.h
                     ${PHXKV_SRC}/kv_grpc_server_main.cpp
                     ${PHXKV_SRC}/kv_paxos.cpp
                     ${PHXKV_SRC}/kv_paxos.h
                     ${PHXKV_SRC}/kvsm.cpp
                     ${PHXKV_SRC}/kvsm.h
                     ${PHXKV_SRC}/log.cpp
                     ${PHXKV_SRC}/log.h
                     ${PHXKV_HDRS} ${PHXKV_HDRS_GRPC}
                     ${PHXKV_SRCS} ${PHXKV_SRCS_GRPC})
add_executable(phxkv_client ${PHXKV_SRC}/kv.cpp
                     ${PHXKV_SRC}/def.h
                     ${PHXKV_SRC}/kv.h
                     ${PHXKV_SRC}/log.cpp
                     ${PHXKV_SRC}/log.h
                     ${PHXKV_SRC}/kv_grpc_client.cpp
                     ${PHXKV_SRC}/kv_grpc_client.h
                     ${PHXKV_SRC}/kv_grpc_client_main.cpp
                     ${PHXKV_HDRS} ${PHXKV_HDRS_GRPC}
                     ${PHXKV_SRCS} ${PHXKV_SRCS_GRPC})
target_link_libraries(phxkv_server ${LINK_SRC_LIBS} ${LINK_PLUGIN_LIBS} ${PROTO_LIBS} ${LINK_DEP_LIBS})
target_link_libraries(phxkv_client ${LINK_SRC_LIBS} ${LINK_PLUGIN_LIBS} ${PROTO_LIBS} ${LINK_DEP_LIBS})
