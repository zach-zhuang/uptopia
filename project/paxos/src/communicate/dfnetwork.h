/*
Tencent is pleased to support the open source community by making 
PhxPaxos available.
Copyright (C) 2016 THL A29 Limited, a Tencent company. 
All rights reserved.

Licensed under the BSD 3-Clause License (the "License"); you may 
not use this file except in compliance with the License. You may 
obtain a copy of the License at

https://opensource.org/licenses/BSD-3-Clause

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" basis, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
implied. See the License for the specific language governing 
permissions and limitations under the License.

See the AUTHORS file for names of contributors. 
*/

#pragma once

#include <string>
#include "phxpaxos/network.h"
#include <io/tcp.hpp>
#include <io/udp.hpp>
#include <thread/block_queue.hpp>
#include <io/tcp.hpp>
#include <base/uri.hpp>
#include <base/mem_buffer.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
using namespace boost::posix_time;
using namespace utopia;

class UDPSend: SR::CallBack
{
public:

    void init(const std::string &ip,const int port)
    {
        Uri uri(ip,port);
        send.bind(uri);
        send.setCallback(this);
        Thread*t = new Thread(&send);
        t->start();
    }

    void sendMessage(const std::string &ip, const int port, const std::string &message)
    {
        uint32_t iIP = (uint32_t) inet_addr(ip.c_str());
        uint64_t llNodeID = (((uint64_t) iIP) << 32) | port;
        Uri *uri = new Uri(ip,port);
        std::map<uint64_t ,IBuffer*>::iterator it = iommap_.find(llNodeID);
        if(it == iommap_.end())
        {
            IBuffer *buf = new MemBuffer;
            buf->clear();
            buf->resize(0);
            iommap_.insert(std::pair<uint64_t,IBuffer*>(llNodeID,buf));
            buf->write((Byte*)message.c_str(),message.size());
            send.sendASyn(buf,uri);
        }else{
            IBuffer* buf = it->second;
            buf->clear();
            buf->resize(0);
            buf->write((Byte*)message.c_str(),message.size());
            send.sendASyn(buf,uri);
        }
    }

    virtual void onSend(SR* sr,const IBuffer* buffer,const Uri* uri,size_t size,int ret)
    {
    }

    virtual void sendDone(SR* sr,const IBuffer* buffer,const Uri* uri,size_t size,int ret)
    {
        delete uri;
    }

private:
    std::map<uint64_t,IBuffer*> iommap_;
    UDPSendAndRece send;
};

class UDPRece:SR::CallBack
{
public:
    UDPRece(phxpaxos::NetWork* net):net_(net){}

    void init(const std::string &ip,const int port)
    {
        Uri* uri = new Uri;
        rece.bind(Uri(ip,port));
        rece.setCallback(this);
        buf.clear();
        buf.resize(1024*64);
        rece.receSomeASyn(&buf,uri);
        Thread*t = new Thread(&rece);
        t->start();
    }

    virtual void receDone(SR* sr,IBuffer* buffer,Uri* uri,size_t size,int ret)
    {
        net_->OnReceiveMessage((const char*)buffer->getConstData(),size);
        buf.clear();
        buf.resize(1024*64);
        rece.receSomeASyn(&buf,uri);
    }
private:
    UDPSendAndRece rece;
    MemBuffer buf;
    phxpaxos::NetWork *net_;
};

class Item
{
public:
    Uri uri;
    uint64_t id;
    std::string msg;
    int count;
};

class TCPSend:public Runnable
{
public:

    TCPSend()
    {
        Thread* t = new Thread(this);
        t->start();
    }

    void run()
    {
        while(true)
        {
            Item item;
            bool ret = queue_.pop(item);
            int err = 0;
            if(ret)
            {
                std::map<uint64_t ,IO*>::iterator it = iomap_.find(item.id);
                IO *io;
                if (it == iomap_.end())
                {
                    io = new SocketClient;
                    io->openSyn(Uri(item.uri.getAddress(),item.uri.getPort()),err);
                    if(err!=0)
                    {
                        item.count++;
                        if(item.count<3)queue_.push(item);
                        delete io;
                        continue;
                    }
                    iomap_.insert(std::pair<uint64_t ,IO*>(item.id,io));
                }else{
                    io = it->second;
                }
                buf.clear();
                buf.resize(0);
                buf.write((Byte*)item.msg.c_str(),item.msg.size());
                int wsize = io->writeSyn(buf,err);
                if(err!=0 || wsize == 0)
                {
                    io->close();
                    delete io;
                    iomap_.erase(item.id);
                    item.count++;
                    if(item.count<3)queue_.push(item);
                }
            }
        }
    }

    void sendMessage(const std::string &ip, const int port, const std::string &message)
    {
        uint32_t iIP = (uint32_t) inet_addr(ip.c_str());
        uint64_t llNodeID = (((uint64_t) iIP) << 32) | port;

        Item item;
        item.count = 0;
        item.uri = Uri(ip,port);
        item.id = llNodeID;
        item.msg = message;
        queue_.push(item);
    }

private:
    std::map<uint64_t,IO*> iomap_;
    BlockQueue<Item> queue_;
    MemBuffer buf;
};

class TCPRece:public IOs::CallBack,public IO::CallBack
{
public:
    TCPRece(phxpaxos::NetWork* net):net_(net){}

    void init(const string& ip,const int port)
    {
        ip_ = ip;
        port_ = port;
        sr.setCallback(this);
        sr.bind(Uri(ip,port));
        sr.acceptASynIOASyn(this);
        utopia::Thread* t = new utopia::Thread(&sr);
        t->start();
    }

    virtual void onAcceptIO(IOs* ios, IO* io,int ret = 0)
    {
        MemBuffer* mem = new MemBuffer;
        mem->resize(1024);
        io->readSomeASyn(mem);
        sr.acceptASynIOASyn(this);
    }

    virtual void readDone(IO* io, IBuffer* buf,size_t size,int ret)
    {
        if(ret!=0){
            cout<<"read error"<<endl;
            delete buf;
            return;
        }
        net_->OnReceiveMessage((const char*)buf->getConstData(),size);
        buf->clear();
        buf->resize(1024);
        io->readSomeASyn(buf);
    }

private:
    string ip_;
    int port_;
    SocketServer sr;
    phxpaxos::NetWork *net_;
};


namespace phxpaxos
{

class DFNetWork : public NetWork
{
public:
    DFNetWork();
    virtual ~DFNetWork();

    int Init(const std::string & sListenIp, const int iListenPort, const int iIOThreadCount);

    void RunNetWork();

    void StopNetWork();

    int SendMessageTCP(const int iGroupIdx, const std::string & sIp, const int iPort, const std::string & sMessage);

    int SendMessageUDP(const int iGroupIdx, const std::string & sIp, const int iPort, const std::string & sMessage);

private:

    TCPRece tcpRece;
    TCPSend tcpSend;
    UDPRece udpRece;
    UDPSend udpS;
};

}
