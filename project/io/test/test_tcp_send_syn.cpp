//
// Created by zoson on 18-10-6.
//

#include <io/tcp.hpp>
#include <base/uri.hpp>
#include <base/mem_buffer.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <thread/block_queue.hpp>

using namespace boost::posix_time;
using namespace utopia;


class Item
{
public:
    Uri uri;
    uint64_t id;
    std::string msg;
    int count;
};

class TCPSend:public Runnable
{
public:

    TCPSend()
    {
        Thread* t = new Thread(this);
        t->start();
    }

    void run()
    {
        while(true)
        {
            Item item;
            bool ret = queue_.pop(item);
            int err = 0;
            if(ret)
            {
                std::map<uint64_t ,IO*>::iterator it = iomap_.find(item.id);
                IO *io;
                if (it == iomap_.end())
                {
                    io = new SocketClient;
                    io->openSyn(Uri(item.uri.getAddress(),item.uri.getPort()),err);
                    cout<<"openSyn "<<err<<endl;
                    if(err!=0)
                    {
                        item.count++;
                        if(item.count<3)queue_.push(item);
                        delete io;
                        continue;
                    }
                    iomap_.insert(std::pair<uint64_t ,IO*>(item.id,io));
                }else{
                    io = it->second;
                }
                buf.clear();
                buf.resize(0);
                buf.write((Byte*)item.msg.c_str(),item.msg.size());
                int wsize = io->writeSyn(buf,err);
                cout<<"writeSyn "<<err<<" "<<wsize<<endl;
                if(err!=0 || wsize == 0)
                {
                    io->close();
                    delete io;
                    iomap_.erase(item.id);
                    item.count++;
                    if(item.count<3)queue_.push(item);
                }
            }
        }
    }

    void sendMessage(const std::string &ip, const int port, const std::string &message)
    {
        uint32_t iIP = (uint32_t) inet_addr(ip.c_str());
        uint64_t llNodeID = (((uint64_t) iIP) << 32) | port;

        Item item;
        item.count = 0;
        item.uri = Uri(ip,port);
        item.id = llNodeID;
        item.msg = message;
        queue_.push(item);
    }

private:
    std::map<uint64_t,IO*> iomap_;
    BlockQueue<Item> queue_;
    MemBuffer buf;
};

int main()
{
    TCPSend send;
    std::string ip = "127.0.0.1";
    int port = 9000;
    while(true)
    {
        string message;// = "abcderfsafdfwefcxvthrtmirtmrbm";
        cin>>message;
        send.sendMessage(ip,port,message);
        boost::this_thread::sleep( millisec(1));
    }

}
