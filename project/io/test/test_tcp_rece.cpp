//
// Created by zoson on 18-10-4.
//

#include <io/tcp.hpp>
#include <base/uri.hpp>
#include <base/mem_buffer.hpp>

using namespace utopia;

class ReceCallback
{
public:

private:

};

class TCPRece:public IOs::CallBack,public IO::CallBack
{
public:
    TCPRece(const string& ip,const int port)
    {
        sr.setCallback(this);
        sr.bind(Uri(ip,port));
        sr.acceptASynIOASyn(this);
        sr.run();
    }

    virtual void onAcceptIO(IOs* ios, IO* io,int ret = 0)
    {
        std::cout<<"onAccept "<<io->getState()<<std::endl;
        MemBuffer* mem = new MemBuffer;
        mem->resize(1024);
        io->readSomeASyn(mem);
        sr.acceptASynIOASyn(this);
    }

    virtual void onHandleIO(IOs*, IO*,int ret = 0)
    {

    }

    virtual void readDone(IO* io, IBuffer* buf,size_t size,int ret)
    {
        std::cout<<"readDone "<<size<<std::endl;
        if(ret!=0){
            cout<<"read error"<<endl;
            delete buf;
            return;
        }
        buf->getMutableData()[size] = '\0';
        std::cout<<(char*)buf->getMutableData()<<std::endl;
        buf->clear();
        buf->resize(1024);
        io->readSomeASyn(buf);
    }

private:
    SocketServer sr;
};

int main()
{
    std::string ip = "127.0.0.1";
    int port = 9000;
    TCPRece rece(ip,port);

}