//
// Created by zoson on 18-9-5.
//

#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>

#include <time.h>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <io/tcp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
#include <iostream>
using namespace std;
using namespace utopia;
using namespace boost::asio;
using namespace boost::posix_time;
IBuffer* rbuffer = new MemBuffer();
IBuffer* sbuffer = new MemBuffer();
int im_size = 624 *352*3;
double total = 0;
int rsize = 0;
cv::Mat im(352,624,CV_8UC3);

class SocketCallback: public IO::CallBack
{
public:
    virtual void onConnect(IO* io,int ret)
    {
        boost::this_thread::sleep( millisec(00));
        cout<<"onConnect"<<endl;
        rbuffer->clear();
        rbuffer->resize(im_size);
        io->readASyn(rbuffer);
    }

    virtual void onRead(IO* io, IBuffer* buf,size_t size,int ret) 
    {
        cout<<"onRead "<<size<<endl;
    }

    virtual void onWrite(IO* io, const IBuffer* buf,size_t size,int ret) {}

    virtual void onClose(IO* io,int ret) {}

    virtual void readDone(IO* io,IBuffer* buf,size_t size,int ret)
    {
        cout<<"readDone "<<size<<endl;
        buf->read(im.data,buf->getSize());
        cv::imshow("rec",im);
        cv::waitKey(1);
        buf->clear();
        buf->resize(im_size);
        io->readASyn(rbuffer);
    }

    virtual void writeDone(IO* io,const IBuffer* buf,size_t size,int ret) {}

    virtual void onError(IO* io) {}
};


int main(int argc, char* argv[]) {
    SocketClient *client = new SocketClient(new SocketCallback);
    rbuffer->resize(im_size);
    sbuffer->resize(4);
    sbuffer->write((const unsigned char*)"123",4);
    client->openASyn(Uri(9001));
    client->run();

}
