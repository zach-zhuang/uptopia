//
// Created by zoson on 18-9-12.
//

#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <opencv2/opencv.hpp>
#include <io/udp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
#include <iostream>

using namespace std;
using namespace boost::asio;
using namespace boost::posix_time;

using namespace utopia;

boost::recursive_mutex cs;
cv::VideoCapture capture("../data/worm.avi");
MemBuffer sbuffer;
MemBuffer sbufferr;
MemBuffer rbuffer;
int im_size = 624 *352*3;
cv::Mat im;

int wsize = 1024*32;

int main()
{
    Uri uri;
    UDPSendAndRece sr(uri);
    //sr.bind(uri);
    Uri rec;
    //server.acceptIOASyn(new SocketCallback);
    std::cout<<"cnn"<<std::endl;
    sbuffer.resize(wsize);
    sbufferr.resize(im_size%wsize);
    rbuffer.resize(4);
    while(1)
    {
        //cout<<sr.receSyn(rbuffer,rec)<<endl;
        capture.read(im);
        cv::imshow("send",im);
        int ssize = 0;
        while(ssize < im_size)
        {
            sr.receSyn(rbuffer,rec);
            rbuffer.clear();
            if(im_size - ssize < wsize)
            {
                ssize += sbufferr.write(im.data+ssize,im_size%wsize);
                sr.sendSyn(sbufferr,rec);
                sbufferr.clear();
            }else{
                ssize += sbuffer.write(im.data+ssize,wsize);
                sr.sendSyn(sbuffer,rec);
                sbuffer.clear();
            }
        }
        cv:cvWaitKey(1);
    }
    std::cout<<"server end"<<std::endl;

    //boost::thread_group threads;
    //threads.create_thread(accept_thread);
    //threads.create_thread(handle_clients_thread);
    //threads.join_all();
}
