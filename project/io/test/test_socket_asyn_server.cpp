//
// Created by zoson on 18-9-5.
//

#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <opencv2/opencv.hpp>
#include <io/tcp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
#include <iostream>
using namespace std;
using namespace boost::asio;
using namespace boost::posix_time;

using namespace utopia;

typedef boost::shared_ptr<IO> client_ptr;
typedef std::vector<client_ptr> Array;
Array clients;
boost::recursive_mutex cs;
std::vector<cv::VideoCapture> caps;
cv::VideoCapture capture("../data/worm.avi");
std::vector<MemBuffer> sbuffers;
MemBuffer rbuffer;
int im_size = 624 *352*3;
IO* io;
cv::Mat im;
int index_ = 0;
int wsize = 0;
Object ob;
class SocketCallback: public IO::CallBack
{
public:
    virtual void onConnect(IO* io,int ret) {}

    virtual void onRead(IO* io, IBuffer* buf,size_t size,int ret) {}

    virtual void onWrite(IO* io, const IBuffer* buf,size_t size,int ret) {}

    virtual void onClose(IO* io,int ret) {}

    virtual void onError(IO* io,int ret) {}

    virtual void readDone(IO* io,IBuffer* buf,size_t size,int ret) {}

    virtual void writeDone(IO* io,const IBuffer* buf,size_t size,int ret)
    {
        cout<<"writeDone"<<endl;
        {
            Sychronized(&ob)
            if(ret!=0)
            {
                std::cout<<ret<<std::endl;
            }else{
                capture.read(im);
                cv::imshow("send",im);
                cv:cvWaitKey(1);
                IBuffer* b = const_cast<IBuffer*>(buf);
                b->clear();
                b->write(im.data,im_size);
                io->writeASyn(b);
            }
        }
    }
};

class ServerCallback:public IOs::CallBack
{
public:
    virtual void onAcceptIO(IOs* ios, IO* io,int ret)
    {
        capture.read(im);
        sbuffers[index_].resize(im_size);
        sbuffers[index_].clear();
        cv::imshow("send",im);
        cv:cvWaitKey(1);
        sbuffers[index_].write(im.data,im_size);
        io->writeASyn(&sbuffers[index_]);
        index_++;
        //io->run();
        ios->acceptASynIOASyn(new SocketCallback);
    }

    virtual void onHandleIO(IOs* ios, IO* io,int ret) {}

    virtual void onError(IO* io,int ret) {}
};

int main()
{
    Uri uri(9001);
    for(int i=0;i<10;i++)sbuffers.push_back(MemBuffer());
    SocketServer server(uri,new ServerCallback);
    rbuffer.resize(4);
    capture.read(im);
    server.acceptASynIOASyn(new SocketCallback);
    server.run();
}