//
// Created by zoson on 18-10-4.
//

#include <io/tcp.hpp>
#include <base/uri.hpp>
#include <base/mem_buffer.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
using namespace boost::posix_time;
using namespace utopia;

class TCPSend:public IO::CallBack
{
public:

    virtual void onConnect(IO* io,int ret)
    {
        IBuffer* mem = iommap_.find(io)->second;
        string msg = iosmap_.find(io)->second;
        io->setCallback(this);
        mem->clear();
        mem->write((Byte*)msg.c_str(),msg.size());
        io->writeASyn(mem);
    }

    void sendMessage(const std::string &ip, const int port, const std::string &message)
    {
        uint32_t iIP = (uint32_t) inet_addr(ip.c_str());
        uint64_t llNodeID = (((uint64_t) iIP) << 32) | port;
        std::map<uint64_t ,IO*>::iterator it = iomap_.find(llNodeID);
        if (it == iomap_.end()) {
            IO *io = new SocketClient(this);
            io->openASyn(Uri(ip,port));
            iomap_.insert(std::pair<uint64_t ,IO*>(llNodeID,io));
            Thread* t = new Thread(io);
            iotmap_.insert(std::pair<uint64_t,Thread*>(llNodeID,t));
            iommap_.insert(std::pair<IO*,IBuffer*>(io,new MemBuffer));
            iotmap_.find(llNodeID)->second->start();
            iosmap_.insert(std::pair<IO*,string>(io,message));
        }else{
            if(it->second->getState()==iostate::STARTED)
            {
                IBuffer* mem = iommap_.find(it->second)->second;
                mem->clear();
                mem->write((Byte*)message.c_str(),message.size());
                it->second->writeASyn(mem);
            } else if(it->second->getState()==iostate::OPENING) {
                cout<<"openning"<<endl;
            }
        }
    }

    virtual void onWrite(IO* io,const IBuffer* buf,size_t size,int ret = 0)
    {
        cout<<"onWrite "<<size<<" "<<ret<<endl;
    };

    virtual void writeDone(IO* io,const IBuffer* buf,size_t size,int ret = 0)
    {
        cout<<"writeDone "<<size<<" "<<ret<<endl;
    };

private:
    std::map<IO*,string> iosmap_;
    std::map<IO*,IBuffer*> iommap_;
    std::map<uint64_t,Thread*> iotmap_;
    std::map<uint64_t,IO*> iomap_;
};

int main()
{
    TCPSend send;
    std::string ip = "127.0.0.1";
    int port = 9000;
    while(true)
    {
        string message;// = "abcderfsafdfwefcxvthrtmirtmrbm";
        cin>>message;
        send.sendMessage(ip,port,message);
        boost::this_thread::sleep( millisec(1));
    }

}