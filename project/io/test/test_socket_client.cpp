#ifdef WIN32
#define _WIN32_WINNT 0x0501
#include <stdio.h>
#endif

#include <opencv2/opencv.hpp>

#include <time.h>  
#include <iostream>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <io/tcp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
#include <iostream>
#include <time.h>

using namespace utopia;
using namespace boost::asio;
IBuffer* rbuffer = new MemBuffer();
IBuffer* sbuffer = new MemBuffer();
int im_size = 624 *352*3;
double total = 0;
SocketClient *client = new SocketClient(Uri(9001));
cv::Mat im(352,624,CV_8UC3);
clock_t start, finish;
double totaltime = 1;
std::string name ;
void read_answer() {
    rbuffer->clear();
    rbuffer->resize(im_size);
    start = clock();
    int ret = 0;
    cout<<"read "<<client->readSyn(*rbuffer,ret)<<endl;
    rbuffer->read(im.data,im_size);
    total += im_size/1024.0/1024.0;
    finish = clock();
    totaltime = (double)(finish - start) / CLOCKS_PER_SEC;
    std::cout<<1/totaltime<<std::endl;
    //memcpy(im.data,(const unsigned char*)rbuffer->getConstData(),rbuffer->getSize());
    cv::imshow("rec"+name,im);
    cv::waitKey(1);
}

void write(const std::string & msg) {
    int ret = 0;
    sbuffer->clear();
    sbuffer->resize(4);
    cout<<"write "<<client->writeSyn(*sbuffer,ret)<<endl;
}

void write_request() {
    write("ask_clients");
}

void run_client()
{
    while ( true) {
        //std::cout<<client->getState()<<" "<<iostate::CLOSED<<endl;
        if(client->getState() == iostate::CLOSED)
        {
            break;
        }
        write_request();
        read_answer();
        //std::cout<<"speed "<<total/(clock()-time)*CLOCKS_PER_SEC<<" mb/s"<<std::endl;
        //boost::this_thread::sleep(boost::posix_time::millisec(millis));
    }
    std::cout << "client terminated " << std::endl;
}

int main(int argc, char* argv[]) {
    clock_t c = clock();
    std::stringstream ss;
    ss<<c;
    ss>>name;
	rbuffer->resize(im_size);
	sbuffer->resize(4);
	sbuffer->write((const unsigned char*)"123",4);
    run_client();
}
