//
// Created by zoson on 18-9-12.
//

#include <opencv2/opencv.hpp>

#include <time.h>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <io/udp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
#include <time.h>
#include <iostream>
using namespace std;
using namespace utopia;
using namespace boost::asio;
IBuffer* rbuffer = new MemBuffer();
IBuffer* rbufferr = new MemBuffer();
IBuffer* sbuffer = new MemBuffer();
int im_size = 624 *352*3;
double total = 0;
int rsize = 0;
cv::Mat im(352,624,CV_8UC3);
int wsize = 1024*32;
clock_t start, finish;
double totaltime = 1;

int main(int argc, char* argv[])
{
    Uri uri(0);
    UDPSendAndRece sr(uri);
    //sr.bind(uri);
    rbuffer->resize(wsize);
    rbufferr->resize(im_size%wsize);
    sbuffer->resize(4);
    sbuffer->write((const unsigned char*)"123",4);
    Uri sd;
    while(1)
    {
        start = clock();
        size_t rsize = 0;
        size_t prsize = 0;
        while(rsize < im_size)
        {
            sr.sendSyn(*sbuffer,sd);
            sbuffer->clear();
            prsize = rsize;
            if(im_size - rsize < wsize)
            {
                rsize += sr.receSyn(*rbufferr,sd);
                rbufferr->read(im.data+prsize,rbufferr->tellg());
                rbufferr->clear();
            }else{
                rsize += sr.receSyn(*rbuffer,sd);
                rbuffer->read(im.data+prsize,rbuffer->tellg());
                rbuffer->clear();
            }
        }
        finish = clock();
        totaltime = (double)(finish - start) / CLOCKS_PER_SEC;
        cout<<1/totaltime<<endl;
        cv::imshow("rec",im);
        cv::waitKey(1);
    }

}