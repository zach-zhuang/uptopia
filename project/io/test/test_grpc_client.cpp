//
// Created by zoson on 18-9-15.
//
#include <opencv2/opencv.hpp>
#include <iostream>
#include <grpc++/grpc++.h>
#include "utopia_grpc.grpc.pb.h"
#include <time.h>
clock_t start, finish;
double totaltime = 1;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
std::unique_ptr<Server::Stub> stub_;
int im_size = 624 *352*3;
cv::Mat im(352,624,CV_8UC3);
int main()
{
    std::string sServerAddress = "127.0.0.1:8003";
    stub_.reset(nullptr);

    stub_ = Server::NewStub(grpc::CreateChannel(sServerAddress,
                                                     grpc::InsecureChannelCredentials()));

    Request oRequest;
    oRequest.set_id(1);

    while(1)
    {
        start = clock();
        Response oResponse;
        ClientContext context;
        stub_->get(&context,oRequest,&oResponse);
        //std::cout<<oResponse.bs().size()<<std::endl;
        memcpy(im.data,oResponse.bs().c_str(),im_size);
        finish = clock();
        totaltime = (double)(finish - start) / CLOCKS_PER_SEC;
        std::cout<<1/totaltime<<std::endl;
        cv::imshow("rec",im);
        cv::waitKey(1);
    }


}