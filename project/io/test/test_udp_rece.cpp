//
// Created by zoson on 18-10-4.
//

#include <io/udp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>

using namespace utopia;
Uri s(8001);
class UDPRece:public Runnable
{
public:

    void init(const std::string &ip,const int port)
    {
        rece.bind(Uri(ip,port));
        Thread*t = new Thread(this);
        t->start();
    }

    void run()
    {
        while(true)
        {
            buf.clear();
            buf.resize(1024*64);
            int ret = rece.receSomeSyn(buf,uri);
            std::cout<<"receDone "<<ret<<" "<<uri.getPort()<<std::endl;
        }
    }
private:
    UDPSendAndRece rece;
    MemBuffer buf;
    Uri uri;
};

int main()
{
    UDPRece rece;
    rece.init(s.getAddress(),s.getPort());
    while(true)
    {

    }
}