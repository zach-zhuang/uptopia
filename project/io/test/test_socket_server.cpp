#ifdef WIN32
#define _WIN32_WINNT 0x0501
#include <stdio.h>
#endif


#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <opencv2/opencv.hpp>
#include <io/tcp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
using namespace std;
using namespace boost::asio;
using namespace boost::posix_time;

using namespace utopia;

typedef boost::shared_ptr<IO> client_ptr;
typedef std::vector<client_ptr> Array;
Array clients;
boost::recursive_mutex cs;
cv::VideoCapture capture("../data/worm.avi");
SocketServer server;
MemBuffer sbuffer;
MemBuffer rbuffer;
int im_size = 624 *352*3;
IO* io;

void read_request(IO* io) 
{
	int ret = 0;
	rbuffer.clear();
	rbuffer.resize(4);
	cout<<"read"<<endl;
	cout<<"read "<<io->readSyn(rbuffer,ret)<<endl;

}

void process_request(IO* io,cv::Mat im)
{
	int ret = 0;
	sbuffer.clear();
	sbuffer.write(im.data,im_size);
	cout<<"write "<<io->writeSyn(sbuffer,ret)<<endl;
	cv::imshow("sen",im);
	cv::waitKey(1);
	//else std::cerr << "invalid msg " << msg << std::endl;
}

void answer_to_client(IO* io,cv::Mat im)
{
	try {
	    read_request(io);
	    process_request(io,im);
	} catch ( boost::system::system_error&) {
	    delete io;
	}
}


void accept_thread() {
	server.bind(Uri(9001));
    while ( true) {
		int ret = 0;
        io = server.acceptSynIOSyn(ret);
		cout<<"io state "<<io->getState()<<endl;
		//Linker linker(io);
    }
}

void handle_clients_thread() {
    while ( true)
	{
        boost::this_thread::sleep( millisec(1));
		const std::vector<IO*>& ios = server.getIOs();
		if(ios.size()==0)continue;
		cv::Mat im;
		capture.read(im);
		for(int i=0;i<ios.size();++i)
		{
			if(ios[i]->getState()==iostate::CLOSED)continue;
			answer_to_client(ios[i],im);
		}
        // erase clients that timed out
    }
}

int main()
{
	sbuffer.resize(im_size);
	rbuffer.resize(4);

	boost::thread_group threads;
    threads.create_thread(accept_thread);
    threads.create_thread(handle_clients_thread);
    threads.join_all();
}