//
// Created by zoson on 18-9-15.
//
#include <opencv2/opencv.hpp>
#include <iostream>
#include <grpc++/grpc++.h>
#include "utopia_grpc.grpc.pb.h"
using grpc::ServerContext;
using grpc::Status;
int im_size = 624 *352*3;
std::string data;
cv::VideoCapture capture("../../data/worm.avi");
cv::Mat im;

class GPCServer: public Server::Service
{
public:
    grpc::Status get(grpc::ServerContext* context,const  Request* request, Response * reply)
    {
        capture.read(im);
        data = std::string((char*)(im.data),im_size);
        //reply->set_data(data);
        reply->set_bs(im.data,im_size);
        cv::imshow("send",im);
        cv::waitKey(1);
        std::cout<<data.size()<<std::endl;
        return Status::OK;
    }
};

int main()
{
    std::string sServerAddress = "192.168.1.4:8003";
    GPCServer gpcServer;
    grpc::ServerBuilder oBuilder;
    oBuilder.AddListeningPort(sServerAddress, grpc::InsecureServerCredentials());
    oBuilder.RegisterService(&gpcServer);
    std::unique_ptr<grpc::Server> server(oBuilder.BuildAndStart());
    server->Wait();
}