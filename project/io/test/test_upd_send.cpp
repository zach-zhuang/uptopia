//
// Created by zoson on 18-10-4.
//

#include <io/udp.hpp>
#include <base/mem_buffer.hpp>
#include <base/uri.hpp>
#include <string>
#include <thread/block_queue.hpp>
using namespace utopia;
MemBuffer buf;
Uri s(8001);

class Item
{
public:
    std::string msg;
    Uri uri;
};

class UDPSend:public Runnable
{
public:

    void init(const std::string &ip,const int port)
    {
        Uri uri(ip,port);
        send.bind(uri);
        //send.setCallback(this);
        Thread*t = new Thread(this);
        t->start();
    }

    void run()
    {
        while(true)
        {
            Item ss;
            bool ret = queue.pop(ss);
            if(ret)
            {
                buf.clear();
                buf.resize(0);
                buf.write((Byte*)ss.msg.c_str(),ss.msg.size());
                int wsize = send.sendSyn(buf,ss.uri);
            }
        }
    }

    void sendMessage(const std::string &ip, const int port, const std::string &message)
    {
        Item item;
        item.msg = message;
        item.uri.setPort(port);
        item.uri.setAddress(ip);
        queue.push(item);
//        uint32_t iIP = (uint32_t) inet_addr(ip.c_str());
//        uint64_t llNodeID = (((uint64_t) iIP) << 32) | port;
//        Uri *uri = new Uri(ip,port);
//        std::map<uint64_t ,IBuffer*>::iterator it = iommap_.find(llNodeID);
//        if(it == iommap_.end())
//        {
//            IBuffer *buf = new MemBuffer;
//            buf->clear();
//            iommap_.insert(std::pair<uint64_t,IBuffer*>(llNodeID,buf));
//            buf->write((Byte*)message.c_str(),message.size());
//            send.sendASyn(buf,uri);
//        }else{
//            IBuffer* buf = it->second;
//            buf->clear();
//            buf->write((Byte*)message.c_str(),message.size());
//            send.sendASyn(buf,uri);
//        }
    }

//    virtual void onSend(SR* sr,const IBuffer* buffer,const Uri* uri,size_t size,int ret)
//    {
//        std::cout<<"onSend"<<std::endl;
//    }
//
//    virtual void sendDone(SR* sr,const IBuffer* buffer,const Uri* uri,size_t size,int ret)
//    {
//        delete uri;
//        std::cout<<"sendDone"<<std::endl;
//    }

private:
    MemBuffer buf;
    //std::map<uint64_t,IBuffer*> iommap_;
    UDPSendAndRece send;
    BlockQueue<Item> queue;
};

int main()
{
    Uri uri;
    UDPSend send;
    send.init(uri.getAddress(),uri.getPort());
    while(true)
    {
        string ss = "heloo";
        //cin>>ss;
        send.sendMessage(s.getAddress(),s.getPort(),ss);
        boost::this_thread::sleep(boost::posix_time::millisec(100));
    }
}