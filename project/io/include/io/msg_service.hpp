//
// Created by zoson on 18-9-16.
//

#ifndef UTOPIA_MSG_SERVICE_HPP
#define UTOPIA_MSG_SERVICE_HPP

#include <io/io.hpp>

namespace utopia
{

class MsgClient: IO
{
public:
    virtual int openSyn(const Uri &uri);
    virtual int readSyn(IBuffer &buf);
    virtual int writeSyn(const IBuffer &buf);
    virtual void readASyn(IBuffer *buf);
    virtual void writeASyn(const IBuffer *buf);
    virtual void close();
    virtual int getState();
    virtual void run();
private:

};

class MsgServer: IOs
{
public:
    virtual int bind(const Uri &uri);
    virtual IO *acceptIO();
    virtual void handleIO();
    virtual void acceptIOASyn(IO::CallBack *cb);
    virtual void handleIOASyn();
    virtual int getState();
    virtual void run();
private:

};

}

#endif //UTOPIA_MSG_SERVICE_HPP
