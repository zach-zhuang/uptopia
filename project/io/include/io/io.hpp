#ifndef _UTOPIA_IO_HPP_
#define _UTOPIA_IO_HPP_

#include <cstddef>
#include <vector>
#include <thread/thread.hpp>
namespace utopia
{

class Uri;
class IBuffer;

namespace iostate
{
    enum State{CLOSED,OPENING,STARTED};
}

class IO:virtual public Runnable,virtual public Object
{
public:
	class CallBack;
	IO(CallBack* cb = NULL):cb_(cb){}
    virtual ~IO() {}
	virtual int openSyn(const Uri& uri,int& ret)=0;
	virtual void openASyn(const Uri &uri) = 0;
	virtual int readSyn(IBuffer &buf,int& ret)=0;
	virtual int readSomeSyn(IBuffer &buf,int& ret)=0;
	virtual int writeSyn(const IBuffer& buf,int& ret)=0;
	virtual int writeSomeSyn(const IBuffer& buf,int& ret)=0;
	virtual void readASyn(IBuffer* buf)=0;
	virtual void readSomeASyn(IBuffer* buf)=0;
	virtual void writeASyn(const IBuffer *buf)=0;
	virtual void writeSomeASyn(const IBuffer *buf)=0;
	virtual void close()=0;
    virtual int getState()=0;
	virtual void run() = 0;
	void setCallback(CallBack * cb){ cb_ = cb; }
	//runnable
	virtual bool isAuto(){return true;}
	class CallBack
	{
	public:
        virtual void onConnect(IO* io,int ret = 0) {};
		virtual void onRead(IO* io, IBuffer* buf,size_t size,int ret = 0) {};
        virtual void readDone(IO* io, IBuffer* buf,size_t size,int ret = 0) {};
		virtual void onWrite(IO* io,const IBuffer* buf,size_t size,int ret = 0) {};
        virtual void writeDone(IO* io,const IBuffer* buf,size_t size,int ret = 0) {};
		virtual void onError(IO* io,int ret = 0) {};
		virtual void onClose(IO* io,int ret = 0) {};
	};

protected:
	IO(const IO&){}
	IO& operator=(const IO&){}

protected:
	CallBack *cb_;
};

class IOProxy:public IO
{
public:
	IOProxy(IO& stub,CallBack* cb):stub_(stub){ stub_.setCallback(cb); }
	virtual int readSyn(IBuffer &buf,int& ret){return stub_.readSyn(buf,ret);}
	virtual int readSomeSyn(IBuffer &buf,int& ret){return stub_.readSomeSyn(buf,ret);}
	virtual int writeSyn(const IBuffer &buf,int& ret){return stub_.writeSyn(buf,ret);}
	virtual int writeSomeSyn(const IBuffer &buf,int& ret){return stub_.writeSomeSyn(buf,ret);}
	virtual void readASyn(IBuffer* buf){return stub_.readASyn(buf);}
	virtual void readSomeASyn(IBuffer* buf){return stub_.readSomeASyn(buf);}
	virtual void writeASyn(const IBuffer* buf){return stub_.writeASyn(buf);}
	virtual void writeSomeASyn(const IBuffer* buf){return stub_.writeSomeASyn(buf);}
	virtual void close(){return stub_.close();}
    virtual int getState(){return stub_.getState();}
	virtual void run(){return stub_.run();}
protected:
	virtual int openSyn(const Uri& uri,int& ret){};
	virtual void openASyn(const Uri &uri) {};
	IO& stub_;
};

class IOs:virtual public Runnable,virtual public Object
{
public:
	class CallBack;
	IOs(CallBack *cb = NULL):cb_(cb){}
    virtual ~IOs() {}
	virtual int bind(const Uri& uri)=0;
	virtual IO* acceptSynIOSyn(int& ret)=0;
	virtual IO* acceptASynIOSyn(IO::CallBack *cb,int& ret)=0;
	virtual void handleIO(int& ret)=0;
	virtual void acceptSynIOASyn()=0;
	virtual void acceptASynIOASyn(IO::CallBack* cb) = 0;
	virtual void handleIOASyn()=0;

	void setCallback(CallBack *cb){cb_ = cb;};
    virtual int getState()=0;
    virtual const std::vector<IO*>& getIOs() = 0;
    virtual void run() = 0;
	//runnable
	virtual bool isAuto(){return true;}
public:
	class CallBack
	{
	public:
		virtual void onAcceptIO(IOs*, IO*,int ret = 0) {};
		virtual void onHandleIO(IOs*, IO*,int ret = 0) {};
		virtual void onError(IO* io,int ret = 0) {};
	};

protected:
	CallBack *cb_;
};

}

#endif