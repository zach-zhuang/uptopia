#ifndef _UTOPIA_IO_NET_HPP_
#define _UTOPIA_IO_NET_HPP_

#include <io/io.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <vector>
#include <map>
namespace utopia
{

class IBuffer;
class Uri;

class SocketServer;
class SocketClient:public IO
{
public:
	friend SocketServer;
    SocketClient();
    SocketClient(CallBack* cb);
	explicit SocketClient(const Uri& uri,CallBack* cb = NULL);
	virtual ~SocketClient();
	virtual int openSyn(const Uri& uri,int& ret);
	virtual void openASyn(const Uri &uri);
	virtual int readSyn(IBuffer &buf,int& ret);
    virtual int readSomeSyn(IBuffer &buf,int& ret);
    virtual int writeSyn(const IBuffer &buf,int& ret);
    virtual int writeSomeSyn(const IBuffer& buf,int& ret);
	virtual void readASyn(IBuffer *buf);
    virtual void readSomeASyn(IBuffer* buf);
    virtual void writeASyn(const IBuffer *buf);
    virtual void writeSomeASyn(const IBuffer *buf);
    virtual int getState();
	virtual void close();
    virtual void run();

    void connectInternal(const boost::system::error_code err);
	void readASynInternal(IBuffer *buf,size_t size,const boost::system::error_code err);
	void readSomeASynInternal(IBuffer *buf,size_t size,const boost::system::error_code err);
	void writeASynInternal(const IBuffer *buf,size_t size,const boost::system::error_code err);
	void writeSomeASynInternal(const IBuffer *buf,size_t size,const boost::system::error_code err);
protected:
	void init();
	void deinit();
protected:
	boost::asio::ip::tcp::socket *sock_;
	boost::asio::io_service service_;
    bool asyn_;
    size_t buf_size_;
private:
	iostate::State state_;
};

class SocketServer:public IOs
{
public:
	SocketServer(CallBack* cb = NULL);
	explicit SocketServer(const Uri& uri,CallBack* cb);
	virtual ~SocketServer();
	virtual int bind(const Uri& uri);
	virtual IO* acceptSynIOSyn(int& ret);
	virtual IO* acceptASynIOSyn(IO::CallBack *cb,int& ret);
	virtual void acceptSynIOASyn();
	virtual void acceptASynIOASyn(IO::CallBack* cb);
    virtual void handleIO(int& ret);
    virtual void handleIOASyn();
	virtual const std::vector<IO*>& getIOs(){return ios_;};
	virtual int getState(){return state_;};
    virtual void run();

    void acceptSynIOASynInternal(IO* io,const boost::system::error_code err);
    void acceptASynIOASynInternal(IO* io,const boost::system::error_code err);
protected:
	SocketServer(const SocketServer&){};

private:
	iostate::State state_;
	boost::asio::ip::tcp::acceptor *aptor_;
	boost::asio::io_service service_;
	std::map<IO*,Thread*> iotmap_;
	std::vector<IO*> ios_;
	boost::recursive_mutex cs_;
    bool asyn_;
};

}


#endif