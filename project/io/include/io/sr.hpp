//
// Created by zoson on 18-10-4.
//

#ifndef IO_SR_HPP
#define IO_SR_HPP

#include <cstddef>
#include <thread/thread.hpp>

namespace utopia
{

class Uri;
class IBuffer;

class SR:virtual public Runnable
{
public:
    class CallBack;
    SR(CallBack* cb = NULL):cb_(cb){}
    virtual int bind(const Uri& uri) = 0;
    virtual int sendSyn(const IBuffer& buf,const Uri& uri) = 0;
    virtual int sendSomeSyn(const IBuffer& buf,const Uri& uri) = 0;
    virtual int receSyn(IBuffer& buf, Uri& uri) = 0;
    virtual int receSomeSyn(IBuffer& buf, Uri& uri) = 0;
    virtual void sendASyn(const IBuffer* buf,const Uri* uri) = 0;
    virtual void sendSomeASyn(const IBuffer* buf,const Uri* uri) = 0;
    virtual void receASyn(IBuffer* buf, Uri* uri) = 0;
    virtual void receSomeASyn(IBuffer* buf,Uri* uri) = 0;
    virtual void close() = 0;
    virtual void run() = 0;
    virtual void setCallback(CallBack* cb) { cb_ = cb;}
    class CallBack
    {
    public:
        virtual void onBind(SR* sr,int ret) {};
        virtual void onSend(SR* sr,const IBuffer* buffer,const Uri* uri,size_t size,int ret) {};
        virtual void sendDone(SR* sr,const IBuffer* buffer,const Uri* uri,size_t size,int ret) {};
        virtual void onRece(SR* sr,IBuffer* buffer,Uri* uri,size_t size,int ret) {};
        virtual void receDone(SR* sr,IBuffer* buffer,Uri* uri,size_t size,int ret) {};
        virtual void onClose(SR* sr,int ret) {};
    };

protected:
    CallBack *cb_;
};


}

#endif //IO_SR_HPP
