//
// Created by zoson on 18-10-4.
//

#ifndef IO_UDP2_HPP
#define IO_UDP2_HPP

#include <io/sr.hpp>
#include <boost/asio.hpp>

namespace utopia
{

class IBuffer;
class Uri;

class UDPSendAndRece:public SR
{
public:
    friend CallBack;
    UDPSendAndRece(const Uri& uri,CallBack* cb = NULL);
    UDPSendAndRece(CallBack *cb = NULL) ;
    virtual ~UDPSendAndRece();
    int bind(const Uri &uri) ;
    int sendSyn(const IBuffer &buf, const Uri &uri) ;
    int sendSomeSyn(const IBuffer& buf,const Uri& uri) ;
    int receSyn(IBuffer &buf, Uri &uri) ;
    int receSomeSyn(IBuffer& buf, Uri& uri) ;
    void sendASyn(const IBuffer *buf, const Uri *uri) ;
    void sendSomeASyn(const IBuffer* buf,const Uri* uri) ;
    void receASyn(IBuffer *buf, Uri *uri) ;
    void receSomeASyn(IBuffer* buf,Uri* uri) ;
    void close() ;
    void run();

    void receASynInternal(IBuffer *buf,Uri* uri,boost::asio::ip::udp::endpoint *ep,size_t size,size_t total);
    void sendASynInternal(const IBuffer *buf,const Uri* uri,size_t size,size_t total);
    void receSomeASynInternal(IBuffer *buf,Uri* uri,boost::asio::ip::udp::endpoint *ep,size_t size,size_t total);
    void sendSomeASynInternal(const IBuffer *buf,const Uri* uri,size_t size,size_t total);
protected:
    void init();
    void deinit();
protected:
    boost::asio::ip::udp::socket *sock_;
    boost::asio::io_service service_;
    bool started_;
    bool asyn_;
    size_t unit_ = 1024*16;
};

}

#endif //IO_UDP2_HPP
