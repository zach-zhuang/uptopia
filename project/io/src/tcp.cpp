
#include <io/tcp.hpp>
#include <base/ibuffer.hpp>
#include <base/uri.hpp>
#include <base/mem_buffer.hpp>

using namespace std;
using namespace boost::asio; 

namespace utopia
{

SocketClient::SocketClient():
        sock_(NULL),state_(iostate::CLOSED),asyn_(false) {}

SocketClient::SocketClient(IO::CallBack* cb):
		sock_(NULL),state_(iostate::CLOSED),asyn_(false),IO(cb){}

SocketClient::SocketClient(const Uri& uri, IO::CallBack* cb):
		sock_(NULL),state_(iostate::CLOSED),asyn_(false),IO(cb)
{
    int ret;
	openSyn(uri,ret);
}

SocketClient::~SocketClient()
{
	deinit();
}

int SocketClient::openSyn(const Uri& uri,int& ret)
{
    if(state_ == iostate::STARTED) return -1;
    if(sock_ != NULL)
    {
        delete sock_;
    }
    sock_ = new ip::tcp::socket(service_);
    boost::system::error_code err;
    ip::tcp::endpoint ep(ip::address::from_string(uri.getAddress()),uri.getPort());
	sock_->connect(ep,err);
    ret = err.value();
    if(ret == 0 )
    {
        sock_->set_option(boost::asio::ip::tcp::no_delay(true));
        sock_->set_option(boost::asio::socket_base::keep_alive(true));
        state_ = iostate::STARTED;
    }else{
        deinit();
        state_ = iostate::CLOSED;
    }
    return ret;
}

void SocketClient::openASyn(const Uri &uri)
{
    if(state_ == iostate::STARTED) return ;
    if(sock_ != NULL)
    {
        delete sock_;
    }
    state_ = iostate::OPENING;
    sock_ = new ip::tcp::socket(service_);
    ip::tcp::endpoint ep(ip::address::from_string(uri.getAddress()),uri.getPort());
    sock_->async_connect(ep,boost::bind(&SocketClient::connectInternal,this,_1));
}

void SocketClient::connectInternal(const boost::system::error_code err)
{
    if(err.value()==0)
    {
        sock_->set_option(boost::asio::ip::tcp::no_delay(true));
        sock_->set_option(boost::asio::socket_base::keep_alive(true));
        state_ = iostate::STARTED;
    }else{
        deinit();
        state_ = iostate::CLOSED;
    }
    if(cb_!=NULL)
    {
        cb_->onConnect(this);
    }
}

int SocketClient::readSomeSyn(IBuffer &buf,int& ret)
{
    if(state_ != iostate::STARTED)return 0;
    CHECK(sock_!=NULL);
    if(sock_==NULL)return -1;
    boost::system::error_code err;
    int rsize = boost::asio::read(*sock_,buffer(buf.getMutableData(),buf.getSize()),err);
    if(err.value()!=0)
    {
        deinit();
        return 0;
    }
    buf.seekg(rsize,IBuffer::Pos::cur);
    return rsize;
}

int SocketClient::readSyn(IBuffer &buf,int& ret)
{
    if(state_ != iostate::STARTED)return 0;
    if(buf.getSize()==0)return 0;
    CHECK(sock_!=NULL);
	if(sock_==NULL)return -1;
	size_t tellg = buf.tellg();
    boost::system::error_code err;
    size_t already_read = sock_->read_some(buffer(buf.getMutableData()+tellg,buf.getSize()-tellg),err);
    if(err.value()!=0)
    {
        deinit();
        return already_read;
    }
	while(already_read < buf.getSize()-tellg)
	{
		already_read += sock_->read_some(
			buffer(buf.getMutableData()+already_read+tellg,buf.getSize()-tellg-already_read),err);
        if(err.value()!=0)
        {
            deinit();
            return already_read;
        }
	}
	buf.seekg(already_read,IBuffer::Pos::cur);
	return already_read;
}

void SocketClient::readSomeASyn(IBuffer *buf)
{
    if(buf == NULL || state_ != iostate::STARTED) {
        if(cb_ != NULL){
            cb_->onRead(this,buf,0,-1);
        }
        return;
    }
    sock_->async_read_some(buffer(buf->getMutableData()+buf->tellg(),buf->getSize()-buf->tellg()),
                           boost::bind(&SocketClient::readSomeASynInternal,this,buf,_2,_1));
}

void SocketClient::readSomeASynInternal(IBuffer *buf, size_t size,const boost::system::error_code err)
{
    if(state_ != iostate::STARTED || err.value()!=0)
    {
        deinit();
        if(cb_!=NULL)
        {
            cb_->readDone(this,buf,size,err.value());
        }
        return;
    }
    buf->seekg(size,IBuffer::Pos::cur);
	if(cb_ != NULL)
	{
		cb_->readDone(this,buf,size);
	}
}


void SocketClient::readASyn(IBuffer *buf)
{
    if(state_ != iostate::STARTED  || buf == NULL || buf->getSize() == 0) {
        if(cb_ != NULL){
            cb_->onRead(this,buf,0,-1);
        }
        return;
    }
	sock_->async_read_some(buffer(buf->getMutableData()+buf->tellg(),buf->getSize()-buf->tellg()),
                            boost::bind(&SocketClient::readASynInternal,this,buf,_2,_1));
}

void SocketClient::readASynInternal(IBuffer *buf,size_t size,const boost::system::error_code err)
{
    if(state_ != iostate::STARTED  || err.value()!=0)
    {
        deinit();
        if(cb_!=NULL)
        {
            cb_->readDone(this,buf,size,err.value());
        }
        return;
    }
	buf->seekg(size,IBuffer::Pos::cur);
	if(buf->tellg()==buf->getSize())
	{
		if(cb_!=NULL)
		{
			cb_->readDone(this,buf,size,err.value());
		}
	}else{
		if(cb_!=NULL)
		{
			cb_->onRead(this,buf,size,err.value());
		}
		sock_->async_read_some(buffer(buf->getMutableData()+buf->tellg(),buf->getSize()-buf->tellg()),
							   boost::bind(&SocketClient::readASynInternal,this,buf,_2,_1));
	}
}

int SocketClient::writeSyn(const IBuffer &buf,int& ret)
{
    if(state_ != iostate::STARTED)return 0;
    if(buf.getSize()==0) return 0;
    boost::system::error_code err;
    size_t wsize = boost::asio::write(*sock_,
									  buffer(buf.getConstData()+buf.tellp(),
											 buf.getSize()-buf.tellp()),err);
    ret = err.value();
    return buf.getSize();
}

int SocketClient::writeSomeSyn(const IBuffer &buf,int& ret)
{
    if(state_ != iostate::STARTED)return 0;
    if(buf.getSize()==0) return 0;
    boost::system::error_code err;
    size_t some = sock_->write_some(buffer(buf.getConstData()+buf.tellp(),
										   buf.getSize()-buf.tellp()),err);
    ret = err.value();
	return some;
}

void SocketClient::writeSomeASyn(const IBuffer *buf)
{
    if(state_ != iostate::STARTED || buf == NULL || buf->getSize() == 0) {
        if(cb_ != NULL){
            cb_->writeDone(this,buf,0,-1);
        }
        return ;
    }
    sock_->async_write_some(buffer(buf->getConstData()+buf->tellp(),buf->getSize()-buf->tellp()),
                            boost::bind(&SocketClient::writeSomeASynInternal,this,buf,_2,_1));
}

void SocketClient::writeSomeASynInternal(const IBuffer *buf, size_t size,const boost::system::error_code err)
{
    if(state_ != iostate::STARTED || err.value()!=0)
    {
        deinit();
        if(cb_!=NULL)
        {
            cb_->writeDone(this,buf,size,err.value());
        }
        return;
    }
	const_cast<IBuffer*>(buf)->seekp(size,IBuffer::Pos::cur);
	if(cb_!=NULL)
	{
		cb_->writeDone(this,buf,size,err.value());
	}
}

void SocketClient::writeASyn(const IBuffer *buf)
{
    if(state_ != iostate::STARTED || buf == NULL || buf->getSize() == 0) {
        if(cb_ != NULL){
            cb_->writeDone(this,buf,0,-1);
        }
        return ;
    }
    sock_->async_write_some(buffer(buf->getConstData()+buf->tellp(),buf->getSize()-buf->tellp()),
                           boost::bind(&SocketClient::writeASynInternal,this,buf,_2,_1));
}

void SocketClient::writeASynInternal(const IBuffer *buf,size_t size,const boost::system::error_code err)
{
	if(state_ != iostate::STARTED || err.value()!=0)
    {
        deinit();
        if(cb_!=NULL)
        {
            cb_->writeDone(this,buf,size,err.value());
        }
        return;
    }
	const_cast<IBuffer*>(buf)->seekp(size,IBuffer::Pos::cur);
	if(buf->tellp()==buf->getSize())
	{
		if(cb_!=NULL)
		{
			cb_->writeDone(this,buf,size,err.value());
		}
	}else{
		if(cb_!=NULL)
		{
			cb_->onWrite(this,buf,size,err.value());
		}
		sock_->async_write_some(buffer(buf->getConstData()+buf->tellp(),buf->getSize()-buf->tellp()),
								boost::bind(&SocketClient::writeASynInternal,this,buf,_2,_1));
	}
}

void SocketClient::init()
{

}

void SocketClient::deinit()
{
    state_ = iostate::CLOSED;
    if(!asyn_ && sock_ != NULL)
	{
		delete sock_;
		sock_ = NULL;
	}
    if(asyn_)service_.stop();
}

int SocketClient::getState()
{
    return state_;
}

void SocketClient::close()
{
    state_ = iostate::CLOSED;
    if(!asyn_ && sock_!=NULL)
	{
		delete sock_;
		sock_ = NULL;
	}else if(asyn_)
    {
        service_.stop();
    }
}

void SocketClient::run()
{
    asyn_ = true;
    boost::asio::io_service::work work(service_);
    service_.run();
    if(sock_ != NULL)
    {
        delete sock_;
        sock_ = NULL;
    }
}

SocketServer::SocketServer(CallBack* cb):IOs(cb),aptor_(NULL),state_(iostate::CLOSED),asyn_(false)
{}

SocketServer::SocketServer(const Uri& uri,CallBack* cb):SocketServer(cb)
{
	bind(uri);
}

SocketServer::~SocketServer()
{
    state_ = iostate::CLOSED;
    map<IO*,Thread*>::iterator iter;
    for(int i=0;i<ios_.size();++i)
    {
        delete ios_[i];
    }
    if(!asyn_ && aptor_!=NULL)
	{
		delete aptor_;
        aptor_ = NULL;
	}
    for(iter = iotmap_.begin(); iter != iotmap_.end(); iter++)
    {
        delete iter->second;
    }
    iotmap_.clear();
    if(asyn_)service_.stop();
}

int SocketServer::bind(const Uri &uri)
{
    state_ = iostate::STARTED;
	aptor_ = new ip::tcp::acceptor(service_, ip::tcp::endpoint(ip::tcp::v4(), uri.getPort()));
}

IO* SocketServer::acceptSynIOSyn(int& ret)
{
    if(state_ != iostate::STARTED)return NULL;
	SocketClient *io =  new SocketClient;
	io->sock_ = new ip::tcp::socket(service_);
    aptor_->accept(*(io->sock_));
    io->state_ = iostate::STARTED;
    io->sock_->set_option(boost::asio::ip::tcp::no_delay(true));
    io->sock_->set_option(boost::asio::socket_base::keep_alive(true));
    io->init();
    {
	    boost::recursive_mutex::scoped_lock lk(cs_);
	    ios_.push_back(io);

	}
    return new IOProxy(*io,NULL);
}

IO* SocketServer::acceptASynIOSyn(IO::CallBack *cb, int& ret)
{
    if(state_ != iostate::STARTED)return NULL;
    SocketClient *io =  new SocketClient;
	io->sock_ = new ip::tcp::socket(io->service_);
	aptor_->accept(*(io->sock_));
    io->state_ = iostate::STARTED;
    io->sock_->set_option(boost::asio::ip::tcp::no_delay(true));
    io->sock_->set_option(boost::asio::socket_base::keep_alive(true));
    io->asyn_ = true;
    io->init();
    {
		boost::recursive_mutex::scoped_lock lk(cs_);
		ios_.push_back(io);
		Thread* t = new Thread(io);
		std::pair<IO*,Thread*> iotpair(io,t);
		iotmap_.insert(iotpair);
		t->start();
	}
	return new IOProxy(*io,cb);
}

void SocketServer::acceptSynIOASyn()
{
    if(state_ != iostate::STARTED)return;
    SocketClient *io =  new SocketClient();
    io->sock_ = new ip::tcp::socket(service_);
    aptor_->async_accept(*io->sock_,boost::bind(&SocketServer::acceptSynIOASynInternal,this,io,_1));
    io->state_ = iostate::STARTED;
    io->sock_->set_option(boost::asio::ip::tcp::no_delay(true));
    io->sock_->set_option(boost::asio::socket_base::keep_alive(true));
    io->init();
    {
		boost::recursive_mutex::scoped_lock lk(cs_);
		ios_.push_back(io);

	}
}

void SocketServer::acceptSynIOASynInternal(IO* io,const boost::system::error_code err)
{
    if(err.value()==0)
    {
        ((SocketClient*)io)->sock_->set_option(boost::asio::ip::tcp::no_delay(true));
        ((SocketClient*)io)->sock_->set_option(boost::asio::socket_base::keep_alive(true));
        iotmap_[io]->start();
    }
    if(cb_!=NULL)
    {
        cb_->onAcceptIO(this,io,err.value());
    }
}

void SocketServer::acceptASynIOASyn(IO::CallBack *cb = NULL)
{
    if(state_ != iostate::STARTED)return;
    SocketClient *io =  new SocketClient(cb);
	io->sock_ = new ip::tcp::socket(io->service_);
	aptor_->async_accept(*io->sock_,boost::bind(&SocketServer::acceptASynIOASynInternal,this,io,_1));
    io->state_ = iostate::STARTED;
    io->asyn_ = true;
    io->init();
    {
		boost::recursive_mutex::scoped_lock lk(cs_);
		ios_.push_back(io);
		Thread* t = new Thread(io);
		std::pair<IO*,Thread*> iotpair(io,t);
		iotmap_.insert(iotpair);
	}
}

void SocketServer::acceptASynIOASynInternal(IO* io,const boost::system::error_code err)
{
    if(err.value()==0)
    {
        ((SocketClient*)io)->sock_->set_option(boost::asio::ip::tcp::no_delay(true));
        ((SocketClient*)io)->sock_->set_option(boost::asio::socket_base::keep_alive(true));
        iotmap_[io]->start();
    }
	if(cb_!=NULL)
	{
		cb_->onAcceptIO(this,io,err.value());
	}
}


void SocketServer::handleIOASyn()
{

}

void SocketServer::handleIO(int& ret)
{
    {
        Sychronized(this)
        boost::recursive_mutex::scoped_lock lk(cs_);
        for(vector<IO*>::iterator iter=ios_.begin(); iter!=ios_.end(); iter++)
        {
            if(((SocketClient*)(*iter))==NULL)
            {
                ios_.erase(iter);
            }else if(((SocketClient*)(*iter))->state_ == iostate::CLOSED){
                ios_.erase(iter);
                delete *iter;
            }else{
                if(cb_ != NULL)
                {
                    cb_->onHandleIO(this,*iter,0);
                }
            }
        }
    }
}

void SocketServer::run()
{
    asyn_ = true;
    boost::asio::io_service::work work(service_);
    service_.run();
    if(aptor_ != NULL)
    {
        delete aptor_;
        aptor_ = NULL;
    }
}

}