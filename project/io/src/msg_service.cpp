//
// Created by zoson on 18-9-16.
//
#include <io/msg_service.hpp>

namespace utopia
{

int MsgClient::openSyn(const Uri &uri)
{
    return 0;
}

int MsgClient::readSyn(IBuffer &buf)
{
    return 0;
}

int MsgClient::writeSyn(const IBuffer &buf)
{
    return 0;
}

void MsgClient::readASyn(IBuffer *buf)
{

}

void MsgClient::writeASyn(const IBuffer *buf)
{

}

void MsgClient::close()
{

}

int MsgClient::getState()
{
    return 0;
}

void MsgClient::run()
{

}

int MsgServer::bind(const Uri &uri)
{
    return 0;
}

IO *MsgServer::acceptIO()
{
    return nullptr;
}

void MsgServer::handleIO()
{

}

void MsgServer::acceptIOASyn(IO::CallBack *cb)
{

}

void MsgServer::handleIOASyn()
{

}

int MsgServer::getState()
{
    return 0;
}

void MsgServer::run()
{

}

}