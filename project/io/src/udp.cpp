//
// Created by zoson on 18-10-4.
//
#include <io/udp.hpp>
#include <base/ibuffer.hpp>
#include <base/uri.hpp>
#include <boost/bind.hpp>

using namespace std;
using namespace boost::asio;

namespace utopia {

UDPSendAndRece::UDPSendAndRece(SR::CallBack *cb) : SR(cb), started_(false),asyn_(false)
{
    ip::udp::endpoint ep(ip::udp::endpoint(ip::udp::v4(), 0));
    sock_ = new ip::udp::socket(service_, ep);
    started_ = true;
}

UDPSendAndRece::UDPSendAndRece(const Uri &uri, CallBack *cb) : SR(cb), started_(false),asyn_(false) {
    //bind(uri);
    ip::udp::endpoint ep(ip::udp::endpoint(ip::udp::v4(), uri.getPort()));
    sock_ = new ip::udp::socket(service_, ep);
    started_ = true;
}

UDPSendAndRece::~UDPSendAndRece() {
    close();
}

int UDPSendAndRece::bind(const Uri &uri) {
    if(started_)close();
    ip::udp::endpoint ep(ip::udp::endpoint(
            ip::address::from_string(uri.getAddress()), uri.getPort()));
    sock_ = new ip::udp::socket(service_, ep);
    started_ = true;
    return 0;
}

int UDPSendAndRece::sendSyn(const IBuffer &buf, const Uri &uri) {
    if(!started_)return 0;
    if(buf.getSize()==0)return 0;
    ip::udp::endpoint ep(ip::udp::endpoint(
            ip::address::from_string(uri.getAddress()), uri.getPort()));
    int unit = 1024 * 16;
    int size = buf.getSize() - buf.tellp();
    int offset = buf.tellp();
    while (size - offset > 0) {
        if (size - offset > unit) {
            offset += sock_->send_to(buffer(buf.getConstData() + offset, unit), ep);
        } else {
            offset += sock_->send_to(buffer(buf.getConstData() + offset, size - offset), ep);
        }
    }
    const_cast<IBuffer &>(buf).seekp(offset, IBuffer::Pos::cur);
    return offset;
}

int UDPSendAndRece::sendSomeSyn(const IBuffer &buf, const Uri &uri)
{
    if(!started_)return 0;
    if(buf.getSize()==0)return 0;
    ip::udp::endpoint ep(ip::udp::endpoint(
            ip::address::from_string(uri.getAddress()), uri.getPort()));
    int unit = 1024 * 16;
    int size = buf.getSize() - buf.tellp();
    int offset = buf.tellp();
    if (size - offset > 0) {
        if (size - offset > unit) {
            offset += sock_->send_to(buffer(buf.getConstData() + offset, unit), ep);
        } else {
            offset += sock_->send_to(buffer(buf.getConstData() + offset, size - offset), ep);
        }
    }
    const_cast<IBuffer &>(buf).seekp(offset, IBuffer::Pos::cur);
    return offset;
}

int UDPSendAndRece::receSyn(IBuffer &buf, Uri &uri)
{
    if(!started_)return 0;
    if (buf.getSize() == 0)return 0;
    CHECK(sock_ != NULL);
    if (sock_ == NULL)return -1;
    size_t tellg = buf.tellg();
    if(buf.getSize() - tellg <=0 )return 0;
    ip::udp::endpoint ep;
    size_t already_read = sock_->receive_from(buffer(buf.getMutableData() + tellg, buf.getSize() - tellg), ep);
    while (already_read < buf.getSize() - tellg) {
        already_read += sock_->receive_from(
                buffer(buf.getMutableData() + already_read + tellg, buf.getSize() - tellg - already_read), ep);
    }
    buf.seekg(already_read, IBuffer::Pos::cur);
    uri.setAddress(ep.address().to_string());
    uri.setPort(ep.port());
    return already_read;
}

int UDPSendAndRece::receSomeSyn(IBuffer &buf, Uri &uri)
{
    if(!started_)return 0;
    if (buf.getSize() == 0)return 0;
    CHECK(sock_ != NULL);
    if (sock_ == NULL)return -1;
    size_t tellg = buf.tellg();
    if(buf.getSize() - tellg <=0 )return 0;
    ip::udp::endpoint ep;
    size_t already_read = sock_->receive_from(buffer(buf.getMutableData() + tellg, buf.getSize() - tellg), ep);
    buf.seekg(already_read, IBuffer::Pos::cur);
    uri.setAddress(ep.address().to_string());
    uri.setPort(ep.port());
    return already_read;
}

void UDPSendAndRece::sendASyn(const IBuffer *buf, const Uri *uri) {
    if (!started_ || buf == NULL || uri == NULL) {
        if (cb_ != NULL) {
            cb_->sendDone(this, buf, uri, 0,-1);
        }
        return;
    }
    ip::udp::endpoint ep(ip::udp::endpoint(
            ip::address::from_string(uri->getAddress()), uri->getPort()));
    if (buf->getSize() - buf->tellp() > unit_) {
        sock_->async_send_to(buffer(buf->getConstData() + buf->tellp(), unit_), ep,
                             boost::bind(&UDPSendAndRece::sendASynInternal, this, buf, uri, _2, 0));
    } else {
        sock_->async_send_to(buffer(buf->getConstData() + buf->tellp(), buf->getSize() - buf->tellp()), ep,
                             boost::bind(&UDPSendAndRece::sendASynInternal, this, buf, uri, _2, 0));
    }
    return;
}

void UDPSendAndRece::sendASynInternal(const IBuffer *buf, const Uri *uri, size_t size, size_t total)
{
    if(!started_)
    {
        if(cb_ != NULL)
        {
            cb_->sendDone(this,buf,uri,total+size,-1);
        }
        return;
    }
    const_cast<IBuffer *>(buf)->seekp(size, IBuffer::Pos::cur);
    ip::udp::endpoint ep(ip::udp::endpoint(
            ip::address::from_string(uri->getAddress()), uri->getPort()));
    if (buf->tellp() == buf->getSize()) {
        if (cb_ != NULL) {
            cb_->sendDone(this, buf, uri, total + size,0);
        }
    } else {
        if (buf->getSize() - buf->tellp() > unit_) {
            sock_->async_send_to(buffer(buf->getConstData() + buf->tellp(), unit_), ep,
                                 boost::bind(&UDPSendAndRece::sendASynInternal, this, buf, uri, _2, total + size));
        } else {
            sock_->async_send_to(buffer(buf->getConstData() + buf->tellp(), buf->getSize() - buf->tellp()), ep,
                                 boost::bind(&UDPSendAndRece::sendASynInternal, this, buf, uri, _2, total + size));
        }
    }

}

void UDPSendAndRece::sendSomeASyn(const IBuffer *buf, const Uri *uri)
{
    if (!started_ || buf == NULL || uri == NULL) {
        if (cb_ != NULL) {
            cb_->onSend(this, buf, uri, 0,-1);
        }
        return;
    }
    ip::udp::endpoint ep(ip::udp::endpoint(
            ip::address::from_string(uri->getAddress()), uri->getPort()));
    if (buf->getSize() - buf->tellp() > unit_) {
        sock_->async_send_to(buffer(buf->getConstData() + buf->tellp(), unit_), ep,
                             boost::bind(&UDPSendAndRece::sendSomeASynInternal, this, buf, uri, _2, 0));
    } else {
        sock_->async_send_to(buffer(buf->getConstData() + buf->tellp(), buf->getSize() - buf->tellp()), ep,
                             boost::bind(&UDPSendAndRece::sendSomeASynInternal, this, buf, uri, _2, 0));
    }
    return;
}

void UDPSendAndRece::sendSomeASynInternal(const IBuffer *buf, const Uri *uri, size_t size, size_t total)
{
    if(cb_!=NULL)
    {
        cb_->onSend(this,buf,uri,total+size,0);
    }
}

void UDPSendAndRece::receASyn(IBuffer *buf, Uri *uri) {
    if (!started_ || buf == NULL || buf->getSize() == 0 || uri == NULL) {
        if (cb_ != NULL) {
            cb_->onRece(this, buf, uri, 0,-1);
        }
        return;
    }
    CHECK(sock_ != NULL);
    if (sock_ == NULL)return;
    size_t tellg = buf->tellg();
    boost::asio::ip::udp::endpoint *ep = new boost::asio::ip::udp::endpoint;
    sock_->async_receive_from(buffer(buf->getMutableData() + tellg, buf->getSize() - tellg), *ep,
                              boost::bind(&UDPSendAndRece::receASynInternal, this, buf, uri,ep, _2, 0));
    return;
}

void UDPSendAndRece::receASynInternal(IBuffer *buf, Uri *uri,boost::asio::ip::udp::endpoint *ep, size_t size, size_t total)
{
    if(!started_)
    {
        uri->setPort(ep->port());
        uri->setAddress(ep->address().to_string());
        delete ep;
        if (cb_ != NULL) {
            cb_->onRece(this, buf, uri, total + size,-1);
        }
        return;
    }
    buf->seekg(size, IBuffer::Pos::cur);
    uri->setAddress(ep->address().to_string());
    uri->setPort(ep->port());
    if (buf->tellg() < buf->getSize()) {
        size_t tellg = buf->tellg();
        sock_->async_receive_from(buffer(buf->getMutableData() + tellg, buf->getSize() - tellg), *ep,
                                  boost::bind(&UDPSendAndRece::receASynInternal, this, buf, uri,ep, _2, total + size));
    } else {
        uri->setPort(ep->port());
        uri->setAddress(ep->address().to_string());
        delete ep;
        if (cb_ != NULL) {
            cb_->onRece(this, buf, uri, total + size,0);
        }
    }
}

void UDPSendAndRece::receSomeASyn(IBuffer* buf,Uri* uri)
{
    if (!started_ || buf == NULL || buf->getSize() == 0 || uri == NULL)
    {
        if (cb_ != NULL) {
            cb_->onRece(this, buf, uri, 0,-1);
        }
        return;
    }
    CHECK(sock_ != NULL);
    if (sock_ == NULL)return;
    size_t tellg = buf->tellg();
    boost::asio::ip::udp::endpoint *ep = new boost::asio::ip::udp::endpoint;
    sock_->async_receive_from(buffer(buf->getMutableData() + tellg, buf->getSize() - tellg), *ep,
                              boost::bind(&UDPSendAndRece::receSomeASynInternal, this, buf, uri,ep, _2, 0));
    return;
}

void UDPSendAndRece::receSomeASynInternal(IBuffer *buf, Uri *uri,boost::asio::ip::udp::endpoint *ep,size_t size, size_t total)
{
    uri->setPort(ep->port());
    uri->setAddress(ep->address().to_string());
    delete ep;
    buf->seekg(total + size,IBuffer::Pos::cur);
    if (cb_ != NULL) {
        cb_->receDone(this, buf, uri, total + size,0);
    }
    return;
}

void UDPSendAndRece::close() {
    if(!started_) return;
    if (!asyn_ || sock_ != NULL) {
        delete sock_;
    }else{
        service_.stop();
    }
    started_ = false;
}

void UDPSendAndRece::run()
{
    asyn_ = true;
    boost::asio::io_service::work work(service_);
    service_.run();
    asyn_ = false;
    if(sock_ != NULL)
    {
        delete sock_;
        sock_ = NULL;
    }
}

}