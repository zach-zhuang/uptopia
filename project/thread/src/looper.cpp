//
// Created by zoson on 18-3-25.
//

#include <thread/looper.hpp>
#include <thread/handler.hpp>

namespace utopia
{

Looper::Looper()
{
    queue_ = new MessageQueue;
    thread_ = Thread::currentThread();
}

Looper::~Looper()
{
    delete queue_;
}

void Looper::prepare()
{
    CHECK(thread_local_.get()==NULL);
    if(thread_local_.get()!=NULL)
    {
        LOG(ERROR)<<"Only one Looper may be created per thread";
        exit(0);
    }
    thread_local_.reset(new Looper);
}

void Looper::loop()
{
    Looper* me = getLooper();
    CHECK(me!=NULL);
    if(me==NULL)
    {
        LOG(ERROR)<<"Looper did't prepare";
        return;
    }
    MessageQueue* queue = me->queue_;
    while(true)
    {
        Message msg;
        if(queue->pop(msg))
        {
            if(msg.target.get()==NULL)
            {
                LOG(INFO)<<"Looper quit";
                return;
            }
            msg.target->dispatchMessage(msg);
        }
    }
}

void Looper::quit()
{
    Message msg;
    queue_->push(msg);
}

ThreadLocal<Looper> Looper::thread_local_;


}