//
// Created by zoson on 18-4-5.
//

#include <thread/handler.hpp>
#include <thread/looper.hpp>

namespace utopia
{

Handler::Handler():
        looper_(*(Looper::getLooper())),queue_(*(looper_.getQueue()))
{}

Handler::Handler(Looper &looper):
        looper_(looper),queue_(*(looper_.getQueue()))
{}

Handler::~Handler() {}

bool Handler::handleMessage(const Message& msg)
{
    return false;
}

void Handler::dispatchMessage(const Message& msg)
{
    if(msg.task.get()!=NULL)
    {
        msg.task.get()->run();
    }else{
        handleMessage(msg);
    }
}

bool Handler::sendMessage(const Message &msg)
{
    return queue_.push(msg);
}

}