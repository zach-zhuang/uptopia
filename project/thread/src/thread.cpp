#include "thread/thread.hpp"

namespace utopia
{

Thread::Thread():
        isRunning_(false),cb_(NULL)
{}

Thread::Thread(Runnable* cb):Thread()
{
    cb_ = cb;
}

Thread::~Thread()
{
    stop();
    if(cb_!=NULL&&!cb_->isAuto())delete cb_;
}

void Thread::stop()
{
    if(isRunning_)
    {
        cppthread_->detach();
        delete cppthread_;
        isRunning_= false;
    }
}

void Thread::start()
{
    isRunning_ = true;
    cppthread_ = new std::thread(std::bind(&Thread::runInThread,this));
}

void Thread::runInThread()
{
    Thread::thread_local_.reset(this);
    if(this->cb_==NULL)
    {
        this->run();//call the callback
    }else{
        this->cb_->run();
    }
    this->isRunning_ = false;
    return ;
}

void Thread::join()
{
    cppthread_->join();
    isRunning_=false;
}

ThreadLocal<Thread> Thread::thread_local_;

}


