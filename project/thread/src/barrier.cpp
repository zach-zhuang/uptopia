#include <thread/barrier.hpp>

namespace utopia
{

void Barrier::open()
{
	MutexLockGuard _l(lock_);
	state_ = OPENED;
	cv_.notifyAll();
}

void Barrier::close()
{ 
	MutexLockGuard _l(lock_);
	state_ = CLOSED;
}

void Barrier::wait() 
{
	MutexLockGuard _l(lock_);
	while(state_==CLOSED)
	{
		cv_.wait();
	}
}

}