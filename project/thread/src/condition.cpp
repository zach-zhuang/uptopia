#include "thread/condition.hpp"

namespace utopia
{

Condition::Condition(MutexLock &mutex):mutex_(mutex)
{
    CHECK(!pthread_cond_init(&cond_, NULL));//条件变量初始化
}

Condition::~Condition()
{
    CHECK(!pthread_cond_destroy(&cond_));
}

void Condition::wait()
{
    assert(mutex_.isLocking());
    CHECK(!pthread_cond_wait(&cond_, mutex_.getMutexPtr()));
    mutex_.restoreMutexStatus();
}

void Condition::notify()
{
    CHECK(!pthread_cond_signal(&cond_));
}

void Condition::notifyAll()
{
    CHECK(!pthread_cond_broadcast(&cond_));
}

}