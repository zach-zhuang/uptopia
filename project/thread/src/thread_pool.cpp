#include "thread/thread_pool.hpp"

namespace utopia
{

ThreadPool::ThreadPool(size_t q,size_t n)
    :mutex_(),
     notEmpty_(mutex_),
     notFull_(mutex_),
     isEmpty_(mutex_),
     queueSize_(q),
     threadsNum_(n),
     isStarted_(false),
     isWaited_(false),
     block_count_(0)
{}

ThreadPool::ThreadPool(const ThreadPool& tp):mutex_(),
     notEmpty_(mutex_),
     notFull_(mutex_),
     isEmpty_(mutex_),
     queueSize_(0),
     threadsNum_(0),
     isStarted_(false),
     isWaited_(false),
     block_count_(0)
{} 

ThreadPool::~ThreadPool()
{
    if(isStarted_)
    {
       stop();
    }
}

void ThreadPool::wait()
{
    {
        MutexLockGuard lock(mutex_);
        if(isStarted_==false)return;
        isWaited_ = true;
        notEmpty_.notifyAll();
        isEmpty_.wait();
        isWaited_ = false;
        notEmpty_.notifyAll();
    }
}

void ThreadPool::waitToStop()
{
    {
        MutexLockGuard lock(mutex_);
        if(isStarted_==false)return;
        isWaited_ = true;
        notEmpty_.notifyAll();
        isEmpty_.wait();
        isStarted_ = false;
        isWaited_ = false;
        notEmpty_.notifyAll();
    }
    std::vector<Thread*>::iterator it = threads_.begin();
    for(;it!=threads_.end();it++)
    {
        (*it)->join();
        delete (*it);
    }
    threads_.clear();
}

void ThreadPool::stop()
{   
    {
        MutexLockGuard lock(mutex_);
        if(isStarted_==false)return;
        isStarted_ = false;
        while(!queue_.empty())
        {
            Runnable *t = queue_.front();
            queue_.pop();
            if(!t->isAuto())delete t;
        }
        notEmpty_.notifyAll();
    }
    std::vector<Thread*>::iterator it = threads_.begin();
    for(;it!=threads_.end();it++)
    {
        if((*it)==NULL)continue;
        (*it)->join(); 
        delete (*it);
    }
    threads_.clear();
} 
void ThreadPool::addTask(Runnable* t)
{   
    if(threadsNum_==0)
    {
       t->run();
    }else
    {
        MutexLockGuard lock(mutex_);
        while(queue_.size()>=queueSize_&&isStarted_)
            notFull_.wait();
        if(!isStarted_)return ;
        queue_.push((t));
        notEmpty_.notify();
    }
}

Runnable* ThreadPool::getTask()
{
    MutexLockGuard lock(mutex_);
    block_count_++;
    while(queue_.empty()&&isStarted_)
    {
        if(isWaited_&&block_count_==threadsNum_)isEmpty_.notifyAll();
        notEmpty_.wait();
    }
    block_count_--;
    Runnable* t = NULL;
    if(!queue_.empty())
    {
        assert(!queue_.empty());
        t=queue_.front();
        queue_.pop();
        notFull_.notify();
    }
    return t;
}

void ThreadPool::run()
{
    while(isStarted_)
    {
        Runnable* t = getTask();
        if(t!=NULL)
        {
            t->run();//run
            if(!t->isAuto())delete t;
        }
    }
}

void ThreadPool::start()
{
    if(isStarted_)return;
    isStarted_=true;
    threads_.reserve(threadsNum_);
    for(size_t i=0;i<threadsNum_;++i)
    {
        threads_.push_back(
            new Thread(this));
        threads_[i]->start();
    }
}

void ThreadPool::setThreadNum(size_t size)
{
    if(size <=0)return ;
    if(size == threadsNum_)return;
    waitToStop();
    threadsNum_ = size;
    setQueueSize(size*2);
    start();
}

void ThreadPool::setQueueSize(size_t size)
{
    if(size <=0)return ;
    queueSize_ = size;
}


}