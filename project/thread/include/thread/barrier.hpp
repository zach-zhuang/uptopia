#ifndef _BARRIER_HPP_
#define _BARRIER_HPP_

#include <thread/condition.hpp>
#include <thread/mutexlock.hpp>

namespace utopia
{

class Barrier
{
public:
	inline Barrier(MutexLock &mutex):
		state_(CLOSED),cv_(lock_){}
	inline ~Barrier(){};
	void open();
	void close();
	void wait();
private:
	enum {OPENED,CLOSED};
	MutexLock lock_;
	mutable Condition cv_;
	volatile int state_;
};

}

#endif