#ifndef __CONDITION_H__
#define __CONDITION_H__
#include "mutexlock.hpp"
#include <pthread.h>
#include <assert.h>

namespace utopia
{

class MutexLock;
class Condition
{
    public:
	Condition(MutexLock &mutex);
	~Condition();
	void wait();
	void notify();
	void notifyAll();
    private:
	pthread_cond_t cond_;
	MutexLock &mutex_;
};

}

#endif
