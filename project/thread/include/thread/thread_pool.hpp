#ifndef __THREADPOLL_H__
#define __THREADPOLL_H__

#include "thread/mutexlock.hpp"
#include "thread/condition.hpp"
#include "thread/thread.hpp"
#include <vector>
#include <queue>
#include <thread/barrier.hpp>

namespace utopia
{

class ThreadPool:public Runnable
{
public:
	ThreadPool(size_t qsize,size_t nthreads);
	~ThreadPool();
	void start();
	void stop();
	void wait();
	void waitToStop();
	void addTask(Runnable* t);
	void setThreadNum(size_t size);
	void setQueueSize(size_t size);
	inline int getThreadNum(){return threadsNum_;}
	inline int getQueueSize(){return queueSize_;}
	Runnable* getTask();

	inline bool isStarted()const{return isStarted_;}
	void run();
	
	inline bool isAuto(){return true;}
private:
	ThreadPool(const ThreadPool&);
	ThreadPool& operator=(const ThreadPool&){}
private:
	mutable MutexLock mutex_;
	Condition notEmpty_;
	Condition notFull_;
	Condition isEmpty_;
	size_t queueSize_;
	std::queue<Runnable*> queue_;

	size_t threadsNum_;
	std::vector<Thread*> threads_;
	volatile bool isStarted_;
	volatile int block_count_;
	volatile bool isWaited_;
};

}


#endif
