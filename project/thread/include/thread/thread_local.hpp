//
// Created by zoson on 18-4-5.
//

#ifndef UTOPIA_THREAD_LOCAL_HPP
#define UTOPIA_THREAD_LOCAL_HPP

#include <boost/thread.hpp>

namespace utopia
{
template <typename T>
class ThreadLocal:public boost::thread_specific_ptr<T>
{};

};

#endif //UTOPIA_THREAD_LOCAL_HPP
