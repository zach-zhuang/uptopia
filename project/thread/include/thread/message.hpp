//
// Created by zoson on 18-4-5.
//

#ifndef UTOPIA_MESSAGE_HPP
#define UTOPIA_MESSAGE_HPP

#include <memory>
#include <base/object.hpp>
#include <glog/logging.h>

namespace utopia
{

class Handler;
class Runnable;

class Message
{
public:
    Message(){}
    Message(int index,std::shared_ptr<Handler> handler)
            :index(index),target(handler){}
    Message(int index,std::shared_ptr<void> object,std::shared_ptr<Handler> handler)
    :index(index),object(object),target(handler){}
    Message(std::shared_ptr<Runnable> task,std::shared_ptr<Handler> handler)
            :task(task),target(handler){}
    Message(std::shared_ptr<Handler> handler)
            :target(handler){}
public:
    int index;
    std::shared_ptr<void> object;
    std::shared_ptr<Handler> target;
    std::shared_ptr<Runnable> task;
};

}

#endif //UTOPIA_MESSAGE_HPP
