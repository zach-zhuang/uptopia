#ifndef __THREAD_H__
#define __THREAD_H__
#include "condition.hpp"
#include "mutexlock.hpp"
#include <base/def.hpp>
#include <thread/thread_local.hpp>
#include <base/object.hpp>
#include <future>
#include <thread>
#include <tuple>
#include <utility>

namespace utopia
{

class Runnable:virtual public Object
{
public:
	virtual void run()=0;
	virtual bool isAuto(){return false;}
};

template<typename F,typename R,typename ...P>
class ASyncTask:public Runnable
{
public:
    typedef std::function<F> Func;
    typedef std::tuple<P...> Args;

    ASyncTask(Func func,P... args)
        : func_(std::move(func)),
          args_(std::move(std::tuple<P...>(args...))) {}

    void run()
    {
        call_with_tuple(args_, std::index_sequence_for<P...>());
    }

    inline R get()
    {
        return ret_;
    }

private:
    template<std::size_t... Is>
    void call_with_tuple(const std::tuple<P...>& tuple,
                               std::index_sequence<Is...>)
    {
        ret_ = std::move(func_(std::get<Is>(tuple)...));
    }
private:
    Func func_;
    Args args_;
    R ret_;
};

class Thread:virtual public Object
{
public:
    Thread();
    Thread(Runnable* cb);
    virtual ~Thread();
    void start();
    void restart();
    void stop();
    virtual void run(){};
    void join();
    void runInThread();
    static Thread* currentThread(){return thread_local_.get();};
private:
    std::thread* cppthread_;
    bool isRunning_;
    Runnable* cb_;

    static ThreadLocal<Thread> thread_local_;

DISABLE_COPY_AND_ASSIGN(Thread)

};

}

#endif 
