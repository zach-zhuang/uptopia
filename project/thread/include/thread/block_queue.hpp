//
// Created by zoson on 18-4-5.
//

#ifndef UTOPIA_BlockQueue_HPP
#define UTOPIA_BlockQueue_HPP

#include <base/object.hpp>
#include <iostream>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
using namespace std;

namespace utopia
{

template<typename T>
class BlockQueue : public Object
{
public:
    BlockQueue(int max_size = 1000) ;
    ~BlockQueue() ;

    void clear() ;
    bool full() ;
    bool empty()  ;
    bool front(T &value)  ;
    bool back(T &value)  ;
    int size()  ;
    int max_size()  ;

    bool push(const T &item) ;
    bool pop(T &item) ;
    bool pop(T &item, int ms_timeout) ;

private:
    T *array_;
    int size_;
    int max_size_;
    int front_;
    int back_;
};

template <typename T>
BlockQueue<T>::BlockQueue(int max_size)
{
    if (max_size <= 0) {
        exit(-1);
    }

    max_size_ = max_size;
    array_ = new T[max_size];
    size_ = 0;
    front_ = -1;
    back_ = -1;
}

template <typename T>
BlockQueue<T>::~BlockQueue()
{
    {
        Sychronized(this)
        if (array_ != NULL)delete[] array_;
    }
}

template <typename T>
void BlockQueue<T>::clear()
{
    {
        Sychronized(this)
        size_ = 0;
        front_ = -1;
        back_ = -1;
    }
}

template <typename T>
bool BlockQueue<T>::full()
{
    {
        Sychronized(this)
        if (size_ >= max_size_) {
            return true;
        }
    }
    return false;
}

template <typename T>
bool BlockQueue<T>::empty()
{
    {
        Sychronized(this)
        if (0 == size_) {
            return true;
        }
    }
    return false;
}

template <typename T>
bool BlockQueue<T>::front(T &value)
{
    {
        Sychronized(this)
        if (0 == size_) {
            return false;
        }
        value = array_[front_];
    }
    return true;
}

template <typename T>
bool BlockQueue<T>::back(T &value)
{
    {
        Sychronized(this)
        if (0 == size_) {
            return false;
        }
        value = array_[back_];
    }
    return true;
}

template <typename T>
int BlockQueue<T>::size()
{
    int tmp = 0;
    {
        Sychronized(this)
        tmp = size_;
    }
    return tmp;
}

template <typename T>
int BlockQueue<T>::max_size()
{
    int tmp = 0;
    Sychronized(this)
    tmp = max_size_;
    return tmp;
}

template <typename T>
bool BlockQueue<T>::push(const T& item)
{
    {
        Sychronized(this)
        if (size_ >= max_size_) {
            notify();
            return false;
        }
        back_ = (back_ + 1) % max_size_;
        array_[back_] = item;
        size_++;
        notify();
    }
    return true;
}

template <typename T>
bool BlockQueue<T>::pop(T &item)
{
    {
        Sychronized(this)
        while (size_ <= 0)
        {
            this->wait();
            return false;
        }
        front_ = (front_ + 1) % max_size_;
        item = array_[front_];
        size_--;
    }
    return true;
}

template <typename T>
bool BlockQueue<T>::pop(T &item,int ms_timeout)
{
    {
        Sychronized(this)
        if (size_ <= 0) {
            this->wait(ms_timeout);
            //return false;
        }
        if (size_ <= 0) {
            return false;
        }
        front_ = (front_ + 1) % max_size_;
        item = array_[front_];
        size_--;
    }
    return true;
}

}

#endif //UTOPIA_MSG_QUEUE_HPP
