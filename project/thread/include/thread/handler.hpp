//
// Created by zoson on 18-4-5.
//

#ifndef UTOPIA_HANDLER_HPP
#define UTOPIA_HANDLER_HPP

#include <thread/message.hpp>
#include <thread/block_queue.hpp>
namespace utopia
{

class Looper;

class Handler
{
public:
    Handler();
    Handler(Looper &looper);
    virtual ~Handler();
    Looper& getLooper(){return looper_;}
    virtual bool handleMessage(const Message& msg);
    virtual void dispatchMessage(const Message& msg);
    virtual bool sendMessage(const Message& msg);
private:
    Looper& looper_;
    BlockQueue<Message>& queue_;
};

}

#endif //UTOPIA_HANDLE_HPP
