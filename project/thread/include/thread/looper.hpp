//
// Created by zoson on 18-3-22.
//

#ifndef UTOPIA_LOOPER_HPP
#define UTOPIA_LOOPER_HPP

#include <thread/thread.hpp>
#include <thread/thread_local.hpp>
#include <thread/msg_queue.hpp>
namespace utopia
{

class Looper
{
public:
    ~Looper();
    static void prepare();
    static void loop();
    void quit();
    Thread& getThread(){return *thread_;};
    MessageQueue* getQueue(){return queue_;};
    static Looper* getLooper(){return (thread_local_.get());};
private:
    Looper();

private:
    Thread* thread_;
    MessageQueue* queue_;
    static ThreadLocal<Looper> thread_local_;

DISABLE_COPY_AND_ASSIGN(Looper)

};


}

#endif //UTOPIA_LOOP_HPP
