
#include <iostream>
using namespace std;
#include <thread/mutexlock.hpp>
#include <gtest/gtest.h>
#include <thread/thread_pool.hpp>
#include <boost/thread.hpp>
using namespace utopia;

#define COUNT ((unsigned long)3000000)
class Process:public Runnable
{
public:
	Process():num(0){}
	void run()
	{
		for(int i=0;i<COUNT;++i)
		{
			num += 1;
		}
	}
	bool isAuto(){return true;}
	unsigned long getNum(){return num;}
private:
	unsigned long num;

};

class ThreadPoolTest:public ::testing::Test
{
public:
	void SetUp()
	{
		tp = new ThreadPool(16,8);
		tp->start();
	}
	void TearDown()
	{
		delete tp;
	}
protected:
	ThreadPool* tp;
};

void MultiThreadsTest(ThreadPool *tp,int num,int type=0)
{
	ASSERT_NE((unsigned long)tp,NULL);
	EXPECT_EQ(tp->getThreadNum(),num);
	ASSERT_GT(num,0);
	Process* pros = new Process[tp->getThreadNum()];
	for(int i=0;i<num;++i)
	{
		tp->addTask(&pros[i]);
	}
	switch(type)
	{
		case 0:
			tp->wait();
			break;
		case 1:
			tp->waitToStop();
			break;
		case 2:
			tp->stop();
			break;
	}
	unsigned long total = 0;
	for(int i=0;i<num;++i)
	{
		total += pros[i].getNum();
	}
	switch(type)
	{
		case 0:
		case 1:
			EXPECT_EQ(total,COUNT*num);
			break;
		case 2:
			EXPECT_NE(total,COUNT*num);
			break;
	}
	delete[] pros;
}

TEST_F(ThreadPoolTest,MultiThreads)
{
	MultiThreadsTest(tp,8);
}

TEST_F(ThreadPoolTest,ReSetThreadNum)
{
	tp->setThreadNum(16);
	MultiThreadsTest(tp,16);
	tp->setThreadNum(4);
	MultiThreadsTest(tp,4);
}

TEST_F(ThreadPoolTest,InterruptTask1)
{
	MultiThreadsTest(tp,tp->getThreadNum(),1);
}


TEST_F(ThreadPoolTest,InterruptTask2)
{
	MultiThreadsTest(tp,tp->getThreadNum(),2);
}

int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc,argv);
	return RUN_ALL_TESTS();
}
