//
// Created by zoson on 18-9-9.
//

#include <iostream>
#include <thread/thread.hpp>

using namespace utopia;

class Para
{
public:
    Para()
    {
        cout<<"init"<<endl;
    }
    Para(const Para&)
    {
        cout<<"copy"<<endl;
    }
    ~Para()
    {
        cout<<"delete"<<endl;
    }
    Para( Para&&)
    {
        cout<<"move"<<endl;
    }
    Para& operator=(const Para&)
    {
        cout<<"copy="<<endl;
        return *this;
    }
    Para& operator=(Para&&)
    {
        cout<<"move="<<endl;
        return *this;
    }
};

Para test(Para *p)
{
    return *p;
}

int main()
{
    int a = 1;
    int b = 2;
    auto f = std::move([&](int x,int y,Para* p)->Para{
        return Para();
    });
    Para *p;
    ASyncTask<Para(int,int,Para*),Para,int,int,Para*> task(f,1,2,p);
    task.run();
    Para p2 = std::move(task.get());

}
