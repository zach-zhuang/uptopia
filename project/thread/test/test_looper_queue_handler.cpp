//
// Created by zoson on 18-4-6.
//

#include <iostream>
#include <thread/looper.hpp>
#include <thread/handler.hpp>
#include <thread/msg_queue.hpp>

using namespace std;
using namespace utopia;

class Task:public Runnable
{
public:
    void run()
    {
        cout<<"task"<<endl;
    }
};

class MyHandler:public Handler
{
public:
    bool handleMessage(const Message& msg)
    {
        string* str = (string*)msg.object.get();
        cout<<*str<<endl;
    }

};
std::shared_ptr<MyHandler> handler = nullptr;
class LooperThread:public Thread
{
public:
    void run()
    {
        Looper::prepare();
        handler= std::shared_ptr<MyHandler>(new MyHandler);
        Looper::loop();
        cout<<"end loop"<<endl;
    }
};

int main()
{
    LooperThread* t = new LooperThread;
    t->start();
    int n = 8;
    while(n--)
    {
        if(n%2)
        {
            std::shared_ptr<string> str(new string);
            cin>>*str;
            Message msg;
            msg.object = str;
            msg.target = handler;
            handler->sendMessage(msg);
        }else{
            std::shared_ptr<Runnable> task(new Task());
            Message msg(task,handler);
            handler->sendMessage(msg);
        }
    }
    Message msg;
    handler->sendMessage(msg);
    t->join();
    return 0;
}