//
// Created by zoson on 18-4-6.
//

#include <thread/thread.hpp>
#include <iostream>

class Thread1;
void thread1();

class Thread1:public utopia::Thread
{
public:
    Thread1(){index = 1;}
    void run()
    {
        utopia::Thread::run();
        thread1();
    }
    int index;
};

void thread1()
{
    Thread1* t = (Thread1*)(utopia::Thread::currentThread());
    if(t==NULL)
    {
        std::cout<<"t == NULL"<<std::endl;
    }
    if(t->index == 1)
    {
        std::cout<<"exit ok"<<std::endl;
    }else{
        std::cout<<"exit error"<<std::endl;
    }
}

int main()
{
    Thread1 *t1 = new Thread1();
    t1->start();
    t1->join();
}