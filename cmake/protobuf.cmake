
function(PROTOC_CPP PROTOC FILE SRCS HDRS)
    get_filename_component(DIR ${FILE} DIRECTORY)
    get_filename_component(FIL ${FILE} NAME_WE)
    set(SRCS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.cc")
    set(HDRS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.h")
    add_custom_command(
            OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.cc"
            "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.pb.h"
            COMMAND  ${PROTOC}
            ARGS -I ${DIR} --cpp_out  ${CMAKE_CURRENT_BINARY_DIR} ${FILE}
            DEPENDS ${FILE} ${PROTOC}
            COMMENT "Running C++ protocol buffer compiler on ${FIL}.proto"
            VERBATIM )
    set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
    set(${SRCS} ${SRCS_T} PARENT_SCOPE)
    set(${HDRS} ${HDRS_T} PARENT_SCOPE)
    list(APPEND ${PROTO_HDRS} ${HDRS} PARENT_SCOPE)
endfunction()

function(PROTOC_CPP_GRPC PROTOC FILE SRCS HDRS PLUGIN)
    get_filename_component(DIR ${FILE} DIRECTORY)
    get_filename_component(FIL ${FILE} NAME_WE)
    set(SRCS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.cc")
    set(HDRS_T "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.h")
    add_custom_command(
            OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.cc"
            "${CMAKE_CURRENT_BINARY_DIR}/${FIL}.grpc.pb.h"
            COMMAND  ${PROTOC}
            ARGS -I ${DIR} --grpc_out=${CMAKE_CURRENT_BINARY_DIR} --plugin=protoc-gen-grpc=${PLUGIN} ${FILE}
            DEPENDS ${FILE} ${PROTOC}
            COMMENT "Running C++ protocol buffer compiler on ${FIL}.proto grpc"
            VERBATIM )
    set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
    set(${SRCS} ${SRCS_T} PARENT_SCOPE)
    set(${HDRS} ${HDRS_T} PARENT_SCOPE)
    list(APPEND ${PROTO_HDRS} ${HDRS} PARENT_SCOPE)
endfunction()

function(FIND_PROTOBUF LIBRARIES)
    find_package(Protobuf REQUIRED)
    if(PROTOBUF_FOUND)
        include_directories(${PROTOBUF_INCLUDE_DIRS})
        set(${LIBRARIES} ${PROTOBUF_LIBRARY} PARENT_SCOPE)
    endif()
endfunction()

function(FIND_PROTOBUF2 LIBRARIES PROTOC)
    find_package(Protobuf REQUIRED)
    if(PROTOBUF_FOUND)
        include_directories(${PROTOBUF_INCLUDE_DIRS})
        set(${LIBRARIES} ${PROTOBUF_LIBRARY} PARENT_SCOPE)
    endif()
endfunction()