function(IMPORT PATH LIB)
    link_directories(${PATH}/build)
    include_directories(${PATH}/include)
    add_custom_command(
        OUTPUT ${LIB}
        COMMAND cd ${PROJECT_SOURCE_DIR} && pwd && mkdir -p ${PATH}/build && cd ${PATH}/build  && cmake .. && make ${LIB} && cd ${PROJECT_SOURCE_DIR}
        COMMENT "Build ${LIB}"
        VERBATIM )
endfunction()