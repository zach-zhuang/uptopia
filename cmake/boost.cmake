function(FIND_BOOST LIBRARIES)
    find_package(Boost REQUIRED system thread)
    if(Boost_FOUND)
        link_directories(${Boost_LIBRARY_DIRS})
        include_directories(${Boost_INCLUDE_DIRS})
        set(${LIBRARIES} ${Boost_LIBRARIES} PARENT_SCOPE)
    endif()
endfunction()

