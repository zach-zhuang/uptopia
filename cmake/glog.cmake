function(FIND_GLOG LIBRARIES)
    set(GLOG_ROOT_DIR "" CACHE PATH "Folder contains Google glog")
    if(WIN32)
        find_path(GLOG_INCLUDE_DIR glog/logging.h
                PATHS ${GLOG_ROOT_DIR}/src/windows NO_DEFAULT_PATH)
        find_path(GLOG_INCLUDE_DIR glog/logging.h
                PATHS ${GLOG_ROOT_DIR}/src/windows)
    else()
        find_path(GLOG_INCLUDE_DIR glog/logging.h
                PATHS ${GLOG_ROOT_DIR}
                NO_DEFAULT_PATH)
        find_path(GLOG_INCLUDE_DIR glog/logging.h
                PATHS ${GLOG_ROOT_DIR})
    endif()
        include_directories(${GLOG_INCLUDE_DIR})
    if(MSVC)
        find_library(GLOG_LIBRARY_RELEASE libglog_static
                PATHS ${GLOG_ROOT_DIR}
                PATH_SUFFIXES Release NO_DEFAULT_PATH)

        find_library(GLOG_LIBRARY_RELEASE libglog_static
                PATHS ${GLOG_ROOT_DIR}
                PATH_SUFFIXES Release)

        find_library(GLOG_LIBRARY_DEBUG libglog_static
                PATHS ${GLOG_ROOT_DIR}
                PATH_SUFFIXES Debug NO_DEFAULT_PATH)

        find_library(GLOG_LIBRARY_DEBUG libglog_static
                PATHS ${GLOG_ROOT_DIR}
                PATH_SUFFIXES Debug)

        set(GLOG_LIBRARY optimized ${GLOG_LIBRARY_RELEASE} debug ${GLOG_LIBRARY_DEBUG})
    else()
        find_library(GLOG_LIBRARY glog
                PATHS ${GLOG_ROOT_DIR}
                PATH_SUFFIXES lib lib64
                NO_DEFAULT_PATH)
        find_library(GLOG_LIBRARY glog
                PATHS ${GLOG_ROOT_DIR}
                PATH_SUFFIXES lib lib64)
    endif()
    set(${LIBRARIES} ${GLOG_LIBRARY} PARENT_SCOPE)
endfunction()