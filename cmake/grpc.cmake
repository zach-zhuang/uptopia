function(FIND_GRPC LIBRARIES)
    find_library(GRPC_LIBRARY NAMES grpc)
    find_library(GRPCPP_LIBRARY NAMES grpc++)
    find_library(GPR_LIBRARY NAMES gpr)
    list(APPEND GPRC_LIBS  ${GRPCPP_LIBRARY} ${GRPC_LIBRARY} ${GPR_LIBRARY})
    set(${LIBRARIES} ${GPRC_LIBS} PARENT_SCOPE)
endfunction()

