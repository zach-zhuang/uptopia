function(FIND_GTEST LIBRARIES)
    find_package(GTest REQUIRED)
    if(GTEST_FOUND)
        include_directories(${GTEST_INCLUDE_DIRS})
        set(${LIBRARIES} ${GTEST_BOTH_LIBRARIES} PARENT_SCOPE)
    endif()
endfunction()

